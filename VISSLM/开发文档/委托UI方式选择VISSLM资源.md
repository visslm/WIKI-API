# 委托UI方式选择VISSLM资源
### 1.访问接口为MainData/ChoiceData，参数说明如下：

| 参数名 | 描述 | 是否必填 |
| ----- | ------ | ------ |
| projectId | 项目ID，待选资源来源项目的id。若不确定，可指定为-1。 | 是 |
| isRelationField | 委托UI窗口是否支持项目的切换。取值范围为true/false,默认值为false。 | 否 |
| nodeType | 待选资源的数据类型名称。若不设置此类型值，则委托UI窗口内可切换选择。 | 否 |
| multiple | 资源是否支持多选。取值范围为true/false，默认值为false。 | 否 |
| allowedContainer | 资源类型。取值范围为Document文档/object对象。不设置则为所有类型。 | 否 |
| showTabs | 资源分页。取值有document文档、tree树形、grid列表，可任意组合，以英文逗号隔开，不设置则显示所有分页。 | 否 |
| showBaseline | 是否支持基线的切换，目前仅在showTabs中的document分页中支持。取值范围为true/false，默认值为false。 | 否 |


### 2.委托UI窗口以postMessage方式将已选资源数据发送到当前窗口，返回数据格式如下：

``` json ```
{   
    &emsp;&emsp;"type": "OK", // OK表点击确定，CANCEL表点击取消   
    &emsp;&emsp;"data": "[selectedList]" //已选资源列表  
}

#### 已选资源数据列表data返回资源数据详细说明如下：

``` json ```
{   
    &emsp;&emsp;"id": "资源ID", 
    &emsp;&emsp;"itemId": "资源编号",    
    &emsp;&emsp;"name": "资源名称",    
    &emsp;&emsp;"nodeType": "资源数据类型",     
    &emsp;&emsp;"attributes": {    
        "[documentId]": "条目所属文档ID",    
        "[baselineId]": "条目所属基线ID"
    }
}

### 3.外部访问条目化对象的接口为ProjectManager/ProjectLayout?proId={对象所属项目ID}&objId={文档ID}&goToObj={'{"ChildId":{条目对象ID},"baselineId":{文档基线ID,可不传,为查看最新内容}}'}
   
### 4.外部访问非条目化对象的接口为 Home/Jump?id={对象ID}&baselineId={基线ID,可不传,为查看最新内容}