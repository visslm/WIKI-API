# 测量数据管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 通用参数

所有接口的通用参数

LoginUser：用户信息

| 参数           | 说明        |
| ------------ | --------- |
| AppName      | 访问服务的应用名称 |
| UToken       | 用户登陆后令牌   |
| UserName     | 用户登录名     |
| UserPassword | 用户密码      |

> UToken与(UserName、UserPassword)二选一，同时存在时使用用户名与密码验证

## 获取测量数据

#### 接口地址：

    http://<server>/alm/rest/api/GetMeasureDatas

#### 请求方式：

    GET

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明          |
| ------ | ------ | ---- | ----------- |
| uid    | string | 是    | 历史项目资产对象Uid |
| UToken | string | 是    | 用户令牌        |

#### 输出结果：

    {"ErrorCode":"0",”ErrorMsg“:"成功","Data":"[]"} 
    *Data:历史项目资产库测量项，测量结果的集合 

## 执行测量活动(测量活动对象化)

#### 接口地址：

    http://<server>/alm/rest/api/MeasureActivityExecute

#### 请求方式：

    post

#### 输入参数：

| 参数     | 类型                             | 是否必填 | 说明      |
| ------ | ------------------------------ | ---- | ------- |
| nodeId | string                         | 是    | 对象Uid   |
| UToken | string                         | 是    | 用户令牌    |
| param  | JSONDictionary<string, string> | 否    | 对象的基本信息 |

#### 输出结果：

    "{\"ReturnMsg\":\"success\",\"ErrorMsg\":\"\"}"

## 定时测量

#### 接口地址：

    http://<server>/alm/rest/api/TimingMeasure

#### 请求方式

    get/post/delete/put

#### 输入参数：

| 参数        | 类型     | 是否必填 | 说明                                                                  |
| --------- | ------ | ---- | ------------------------------------------------------------------- |
| frequency | string | 是    | 执行频率(如：Day/Week/TwoWeek/Month/Quarter/Stageend/Milestone/Projectend |
| param     | string | 否    | 空                                                                   |
| nodeId    | string | 是    | 对象Id                                                                |
| UToken    | string | 是    | 用户令牌                                                                |

#### 输出结果：

    无返回值

## 删除测量活动(对象化)

#### 接口地址：

```
http://<server>/alm/rest/ma/DelMeasureActivity
```

#### 请求方式

```
get/post/delete/put
```

#### 输入参数：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| nodeId | string | 是    | 对象Id |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

```
无返回值
```

## 执行测量活动(对象化)

#### 接口地址：

```
http://<server>/alm/rest/ma/MeasureActivityExecute?nodeId={uid}&UToken={utoken}
```

#### 请求方式

```
post
```

#### 输入参数：

| 参数     | 类型                            | 是否必填 | 说明       |
| ------ | ----------------------------- | ---- | -------- |
| nodeId | string                        | 是    | 对象Id     |
| UToken | string                        | 是    | 用户令牌     |
| param  | JSONDictionary<string,string> | 否    | 对象属性及属性值 |

#### 输出结果：

```
无返回值
```
