# 数据管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 自动监控数据管理计划追踪字段，并更新追踪记录

#### 接口地址：

    http://<server>/alm/rest/api/AutoMonitorDMPTraceField

#### 请求方式：

    post

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| uid    | string | 是    | 条目id |
| utoken | string | 是否   | 用户令牌 |

#### 输出结果：

    true/false

## 通过对象Id创建存放路径

#### 接口地址：

    http://<server>/alm/rest/api/CreateStoreByNodeId

#### 请求方式：

    get/post/delete/put

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明                                 |
| ------ | ------ | ---- | ---------------------------------- |
| nodeId | string | 是    | 对象id                               |
| proId  | string | 是    | 项目id                               |
| UToken | string | 是    | 用户令牌                               |
| otype  | string | 否    | 默认值"0", 操作类型(新建="0", 修改="1", 策略使用) |

#### 输出结果：

    "DM目录Code"
