# 文档管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 文档导入需求库(组织资产)

#### 接口地址：

    http://<server>/alm/rest/api/CopyDoc

#### 请求方式：

    Get

#### 参数输入：

| 参数       | 类型     | 是否必填 | 说明                            |
| -------- | ------ | ---- | ----------------------------- |
| docUid   | string | 是    | 要导入到需求库(组织资产)文档对象uid(多个以逗号分隔) |
| parentId | string | 是    | 需求库集合id                       |
| UToken   | string | 是    | 用户令牌                          |

#### 输出结果：

    class:ResultPropertyList   
    包含属性
    {"ErrorCode":0,"ErrorMessage":"xxx message","propList":[]}

# 
