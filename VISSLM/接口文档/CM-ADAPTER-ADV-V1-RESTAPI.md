# 版本管理工具适配器高级接口-RestAPI接口说明 V1.00

此接口为所有支持VISSLM内部代码仓库的版本工具适配器公用, 接口实现形式为 HTTP Restful

**URI格式说明**

    http://user:password@location:port/path?param=value#flag

>返回值格式默认是"application/json", 本文示例中使用伪json示意，请忽略语法错误。

**返回形式**

| 返回类型      | 说明                                                                                                             |
| --------- |:-------------------------------------------------------------------------------------------------------------- |
| GET       | Access one or more resources and return the result as JSON.                                                    |
| POST      | Return 201 Created if the resource is successfully created and return the newly created resource as JSON.      |
| GET / PUT | Return 200 OK if the resource is accessed or modified successfully. The (modified) result is returned as JSON. |
| DELETE    | Returns 204 No Content if the resource was deleted successfully.                                               |

**状态码**

| 返回值                    | 说明                                                                                                                                                     |
| ---------------------- |:------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 200 OK                 | The GET, PUT or DELETE request was successful, and the resource(s) itself is returned as JSON.                                                         |
| 204 No Content         | The server has successfully fulfilled the request, and there is no additional content to send in the response payload body.                            |
| 201 Created            | The POST request was successful, and the resource is returned as JSON.                                                                                 |
| 304 Not Modified       | The resource hasn't been modified since the last request.                                                                                              |
| 400 Bad Request        | A required attribute of the API request is missing. For example, the title of an issue is not given.                                                   |
| 401 Unauthorized       | The user isn't authenticated. A valid user token is necessary.                                                                                         |
| 403 Forbidden          | The request isn't allowed. For example, the user isn't allowed to delete a project.                                                                    |
| 404 Not Found          | A resource couldn't be accessed. For example, an ID for a resource couldn't be found.                                                                  |
| 405 Method Not Allowed | The request isn't supported.                                                                                                                           |
| 409 Conflict           | A conflicting resource already exists. For example, creating a project with a name that already exists.                                                |
| 412                    | The request was denied. This can happen if the If-Unmodified-Since header is provided when trying to delete a resource, which was modified in between. |
| 422 Unprocessable      | The entity couldn't be processed.                                                                                                                      |
| 429 Too Many Requests  | The user exceeded the application rate limits.                                                                                                         |
| 500 Server Error       | While handling the request, something went wrong on the server.                                                                                        |

**错误方式**

    错误时返回状态码404（资源不存在），401（授权错误），500, ...
    响应（respond）中返回错误信息：
     { "type":"error", "result_code": "-1", "result_message": "xxx Error."}
     { "type":"error", "result_code": "404", "result_message": "404 Not Found, ID for a resource couldn't be found."}

**分页方式**

| 参数       | 说明                                                        |
| -------- |:--------------------------------------------------------- |
| page     | Page number (default: 1).                                 |
| per_page | Number of items to list per page (default: 20, max: 100). |

**分页返回头信息**

| Header        | 说明                                             |
| ------------- |:---------------------------------------------- |
| x-next-page   | The index of the next page.                    |
| x-page        | The index of the current page (starting at 1). |
| x-per-page    | The number of items per page.                  |
| X-prev-page   | The index of the previous page.                |
| x-total       | The total number of items.                     |
| x-total-pages | The total number of pages.                     |

    HTTP/2 200 OK
    cache-control: no-cache
    content-length: 1103
    content-type: application/json
    date: Mon, 18 Jan 2016 09:43:18 GMT
    status: 200 OK
    vary: Origin
    x-next-page: 3
    x-page: 2
    x-per-page: 3
    x-prev-page: 1
    x-total: 8
    x-total-pages: 3

**认证方式**

| 认证方式                            | 说明             |
| ------------------------------- |:-------------- |
| *用户名密码，从URL中的user:password获取*   | 普通版本接口支持，不推荐使用 |
| 用户名密码，从Header中的user,password获取  | 高级接口支持，推荐使用    |
| 安全访问授权 {AccessToken} 从Header中获取 | 管理接口必须         |

## 与普通版本工具接口差异

**兼容**

高级适配器默认包含与支持普通适配器接口（普通适配器接口说明详见 CM-RESTAPI 2.x）

**修订版本**

适配器统一使用 revision（修订号） 作为修订版本标记，实际值为版本工具自身特定的标记（如svn-revision，git-commitID，...），不再支持version，pversion等版本标记。

**接口路径**

高级api都位于专用路径v1下，这里的v1从api/about中的advanced中获取到的一致。

## 适配器版本说明 for 高级版本

获取适配器的版本说明
URI

    http://<server>/cmadapter/api/about

**方法:GET** 

返回值

返回版本信息 
```json
{
    "name":"<适配器名称>", 
    "version":"<版本>", 
    "description":"<描述>",
    "advanced":"v1",
    "adapter-type":"cm-adapter",
    "repo-type":"svn"
}
```
Or  
错误返回 HttpException,状态码 500

- advanced 如果有值表示支持高级API，目前固定为v1，作为高级api路径前缀；
- adapter-type 适配器的类型cm-adapter表示配置管理适配器；
- repo-type 仓库类型（svn,git,gitlab,...）。

>svn 类型适配器不支持分支，标签等操作  
>git 普通git类型不支持合并请求

## 功能接口

### 项目文件资源

----------

访问项目中的文件资源，功能类似 `http://<server>/<cmadapter>/resouce?target=<path>`，但参数有调整

URI
```
http://<server>/<cmadapter>/v1/projects/{*pname}/tree/{branch}/{*path}
或
http://<server>/<cmadapter>/v1/projects/{*pname}/-/tree/{branch}/{*path}
```
- pname 包含分组路径的项目名，如: `group1/project1`, 可以是多层级项目名；<br>
    >为了方便从路径中分解出项目名，可以在路径中增加一个特殊的分段 `/-/` 来辅助截断，如： 
        ```
        http://server/cmadapter/v1/projects/group1/project1/-/tree/.../...
        ```  
        **推荐使用带`/-/`的请求地址, 原来的老接口将逐步过度为带`/-/`的.**   

    >目前不同的适配器对`/-/`的支持有区别. 具体为:  
    > - svn适配器已支持.
    > - gitlab适配器已支持.
    > - git适配器暂不支持.
- branch 可以是分支，对于svn来说都是目录，因此branch可以为空，指向仓库根目录，git则必须有branch；
- path 资源路径，可以目录也可以是文件, 多级路径，如 /folder1/folder2/file1.txt, 如文件/目录名中有**特殊字符需编码**。 

**参数清单**

| 参数                      | 说明                                                                      |
| ----------------------- |:----------------------------------------------------------------------- |
| revision                | svn revision, git commit id，缺省默认为最新                                     |
| return=summary (默认)     | 缺省值，返回文件结构概要信息                                                          |
| return=show (可选)        | 返回本身的显示信息                                                               |
| return=file (可选)        | 返回资源文件，目录则返回所有的子文件的压缩文件                                                 |
| return=log (可选)         | 返回历史纪录，默认取最新的100条 pversion 仅针对产品库有效,返回产品版本,version仅指配置项版本               |
| return=list (可选)        | 返回资源文件清单列表                                                              |
| exprops                 | 标准属性之外的其他扩展属性，如："规模，创建人"，与具体工具相关, 缺省为空，不返回扩展属性                          |
| zip=true (可选)           | return=data时，将返回数据压缩 return=file时, 返回压缩文件(目录无视开关,都是压缩文件)                |
| start (可选)              | 起始记录版本号，默认-1，表示最新版本 仅单return=log时有效，                                    |
| end (可选)                | 结束记录版本号，默认0，表示初始版本；start〉end时表示从大的版本往回取，反之表示从小的版本记录往后取； 仅单return=log时有效 |
| limit (可选)              | 返回最大纪录数，默认为50，表示最多返回50条历史记录 仅单return=log时有效                             |
| returnactions=true (可选) | 仅单return=log时有效，将返回版本库每条日志的actions的信息                                   |

**返回示例**

| 参数               | 说明        |
|:---------------- |:--------- |
| return=show (可选) | 返回本身的显示信息 |

返回值                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
```json
{ 
    "short":"任务书/任务书.docx", 
    "full": "[branch]任务书/任务书.docx", 
    "project": "project name", 
    "library-type":"integrated",
    "branch": "trunk", 
    "root-path":"/", 
    "relative-path" : "任务书/任务书.docx"
}  
```
***

| 参数                    | 说明                                                                |
|-----------------------|:------------------------------------------------------------------|
| return=summary (默认)  | 缺省值，返回文件结构概要信息                                                    |
| extend_data           | 扩展默认不返回的其他内容, 比如`message`,`version`. 多个用英文逗号分割. <br/>亦可赋值`all`要求返回所有内容 |

返回值 
```json
{
	"type": "dir",
	"name": "folder1",
	"revision": "2223",
	"total": 3,
	"date": "2016-05-06 12:00:00",
	"member": [
		{
			"type": "file",
			"name": "任务书",
			"resource": "<CM-URI>",
			"revision": "1033",
			"length": 2223,
			"date": "2016-05-06 12:00:00",
			"author": "user",
			......
		}
	]
}
```       
或
```json
{
    "type": "file",
    "name": "abc.c",
    "revision": "1033",
    "length": 2223,
    "date": "2016-05-06 12:00:00",
    "author": "user",
    ......
}
```
***

| 参数               | 说明                      |
|:---------------- |:----------------------- |
| return=file (可选) | 返回资源文件，目录则返回所有的子文件的压缩文件 |

返回值  文件流 

***

| 参数              | 说明                                                                      |
|:--------------- |:----------------------------------------------------------------------- |
| return=log (可选) | 返回历史纪录，等价于 ../v1/projects/{pname}/commits 接口                            |
| start (可选)      | 起始记录版本号，默认-1，表示最新版本 仅单return=log时有效，                                    |
| end (可选)        | 结束记录版本号，默认0，表示初始版本；start〉end时表示从大的版本往回取，反之表示从小的版本记录往后取； 仅单return=log时有效 |
| limit (可选)      | 返回最大纪录数，默认为50，表示最多返回50条历史记录 仅单return=log时有效                             |

返回值

```json
[
	{
		"type": "log",
		"revision": "1003",
		"date": "2016-5-6 7:30",
		"author": "user",
		"message": "message",
		"actions": […]
	}
]
```
 *** 

| 参数          | 说明         |
| ----------- |:---------- |
| return=list | 返回仓库文件列表清单 |

返回值 

```json
[
  "AccountController.cs",
  "right.sh",
  "perftest_erc32/xx/main.c",
  "perftest_erc32/xx/loop.S",
  "perftest_erc32/xx/loop - 副本.S",
  "perftest_erc32/xx/perftest_erc32.vp",
  "中文 123/deploy.sh",
  "aaa.bat",
  "testccs5/run.bat"
]
```

***

**方法:POST**

#### 新建, 仅用于目录资源

| 参数                 | 说明                                                                                   |
|:------------------ |:------------------------------------------------------------------------------------ |
| replace-all = true | 对于目录资源如果设置了这个参数，那么将替换目录下所有原内容为最新内容(添加新的,更新已有的,删除没有的)，具体参考Webservice中的参数说明，默认参数为false |

提交JSON字符串 ContentType=”application/json” 

如果使用Form方式，ContentType=”application/x-www-form-urlencoded”,同时把JSON字符串放在 param字段中 Request[“param”] 

url 上传文件的地址，适配器主动获取; 支持url作为一个目录，获取的文件流返回头部如果包含"visslmfiletype"并且值为"zipdir"时， 那么代表返回的数据流文件是一个目录的压缩文件，name为目录的名字，目录的内容就是压缩包里的文件内容。

    新建目录 
        {"type":"dir","name":"folder","message":"message…"}  
    新建文件 
        {"type":"file","name":"file",data:"base64","message":"message…"} 
         Or 
        {"type":"files", ,urls:[ {"name":"file","url":"url"}],"message":"message…"}
         Or 
        {"type":"file","name":"file",url:"url","message":"message…"} 
    压缩包方式, 可以有文件和目录, 不包含当前目录 
        { type:"zip", zip:"<base64>", "message":"message…"} or { type:"zip", url:"url", "message":"message…"} 

返回值

    返回新建信息, 返回版本信息
    {"type":"dir","name":"source","resource":"<URI>","revision":"12" } 
    or 
    { "type":"file","name":"file","resource":"<URI>",length;"2223",lmt:"2016-5-6",author:"user","revision":"12"} 
    Or 
    {"type":"more","resource":"<COMMIT_URI>","revision":"12"} 
    
    新建zip多条时的返回,通过<COMMIT_URI>能取到提交的细节内容(增删改)。  
    
    {"type":"error", "result_code": "-1","result_message": "" }
    
    result_code 返回码，正常可以不用返回，默认值0；如果有特殊情况，比如SVN的内容一样不能入库，那么返回一个”100”表示没有变化
    result_message 返回描述，正常可以不用返回，默认值空

**方法:PUT 更新资源自身**

提交数据 JSON字符串，同POST方法 如果指定了name,并且与资源现有名字不同,那么使用新的名字重命名资源

| 参数                 | 说明                                                                                   |
|:------------------ |:------------------------------------------------------------------------------------ |
| replace-all = true | 对于目录资源如果设置了这个参数，那么将替换目录下所有原内容为最新内容(添加新的,更新已有的,删除没有的)，具体参考Webservice中的参数说明，默认参数为false |

    更新目录 (改名)
        {"type":"dir","name":"new_name", url:"url" ,"message":"message…"}  
    更新文件 
        {"type":"file",data:"base64","message":"message…"} 
        Or 
        {"type":"file","name":"file",url:"url","message":"message…"}
    压缩包方式, 可以有文件和目录, 不包含当前目录 
        { type:"zip", "name"："new_name", zip:"<base64>", "message":"message…"} or { type:"zip", url:"url", "message":"message…"} 

返回值

    返回新建信息, 返回版本信息
    {"type":"dir","name":"source","resource":"<URI>","revision":"12" } 
    or 
    { "type":"file","name":"file","resource":"<URI>",length;"2223",lmt:"2016-5-6",author:"user","revision":"12"} 
    Or 
    {"type":"more","resource":"<COMMIT_URI>","revision":"12"} 
    
    新建zip多条时的返回,通过<COMMIT_URI>能取到提交的细节内容(增删改)。  
    
    {"type":"error", "result_code": "-1","result_message": "" }
    
    result_code 返回码，正常可以不用返回，默认值0；如果有特殊情况，比如SVN的内容一样不能入库，那么返回一个”100”表示没有变化
    result_message 返回描述，正常可以不用返回，默认值空

**方法:DELETE 删除** 

直接删除URL对应资源   

| 参数      | 说明                                   |
|:------- |:------------------------------------ |
| message | 删除操作的行为记录                            |
| delself | true/false, 是否删除目录本身,默认为true,仅为文件夹有效 |

    删除目录 {"type":"dir","name":"source","message":"message…"} 
    删除文件 {"type":"file","name":"file","message":"message…"}

返回值

    返回新建信息 {"revision":"12"} 失败返回状态码500

***

### 异步任务接口结果查询
部分异步接口请求后, 任务完成前会返回任务ID并进行跳转请求, 直到任务结束时返回任务执行结果

#### URI
```
http://<server>/<cmadapter>/v1/task/{taskId}
```

#### 请求方法
GET

#### 参数
| 参数             | 是否必要 | 说明                                           |
|:---------------|------|:---------------------------------------------|
| taskId         | 是    | 任务ID                                         |


#### 返回值
任务未结束时, 返回302跳转.  
任务完成时, 返回任务结果数据.  
任务失败时, 返回失败结果数据.  
任务不存在时, 返回如下信息:
```json
{
  "result_code": 10004,
  "result_message": "TASK_NOT_FOUND"
}
```
- result_code 返回码，正常默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

### 上传文件
上传文件到代码仓库中, 可以新增文件或修改问题

#### URI
```
http://<server>/<cmadapter>/v1/projects/{*pname}/-/checkin/{branch}/{*path}
```

#### 请求方法
POST(Form)

#### 参数
| 参数             | 是否必要 | 说明                                           |
|:---------------|------|:---------------------------------------------|
| pname          | 是    | 仓库路径                                         |
| branch         | 否    | 仓库分支, SVN仓库无需此参数                             |
| path           | 是    | 入库文件的路径地址                                    |
| commit_message | 是    | 入库提交信息                                       |
| upload         | 是    | 要入库的文件                                       |
| extract_zip    | 否    | 如果file为压缩包, 是否解压file, 再上传压缩包内的所有文件. 默认为false |


#### 返回值
```json
{
  "result_code": 0,
  "result_message": "success"
}
```
- result_code 返回码，正常默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

### 项目文件修改记录资源

----------

读取修改记录信息,{revision}提交版本号

#### URI

        http://<server>/<cmadapter>/v1/projects/{pname}/commits

or

        http://<server>/<cmadapter>/v1/projects/{pname}/commits/{branch}/{*path}

branch 对于git为分支，未传默认取主干（默认分支），svn中与后续的path合并处理，可以为空；
path 可以路径中指定，也可以通过path参数传递，默认以参数为准。

**方法: GET**

| 参数         | 说明                                                                                                       |
|:---------- |:-------------------------------------------------------------------------------------------------------- |
| start (可选) | 起始记录版本号，默认-1，表示最新版本； git无效                                                                               |
| end (可选)   | 结束记录版本号，默认0，表示初始版本；（svn）start〉end时表示从大的版本往回取，反之表示从小的版本记录往后取； git中可代替revision使用                           |
| revision   | 提交号，显示在此之前的所有提交                                                                                          |
| limit (可选) | 返回最大纪录数，默认为50，表示最多返回50条历史记录                                                                              |
| path       | 文件路径，显示所有与此路径相关的修改记录，默认为空，查询整个项目分支下的日志, 指定参数时以参数为准，否则从路径解析。 git 中必须在路径中指定 branch，或者通过commit id间接指名branch |
| page       | 页码 (default: 1).                                                                                         |
| per_page   | 等价于 limit .                                                                                              |

返回值 

    {
        "type":"commits",
        "project": "group1/prj1", 
        “branch”: ”trunk”, 
        “root-path”:”/”, 
        "relative-path" : "folder1/",
        "revision" : "",
        "commits":
            [
              {
                "type":"log",                                         //内置参数
                "revision":"ed899a2f4b5",                            //内置参数
                "message": "Replace sanitize with escape once",        //内置参数
                "author": "Example User",                            //内置参数
                "date":"2016-5-6 7:30",                                //内置参数
                "id": "ed899a2f4b50b4370feeea94676502b42383c746",    //git特有
                "short_id": "ed899a2f4b5",                            //git特有
                "title": "Replace sanitize with escape once",        //git特有
                "committer_name": "Administrator",                    //git特有
                "committer_email": "admin@example.com",                //git特有
                "committed_date": "2012-09-20T11:50:22+03:00",        //git特有
                "created_at": "2012-09-20T11:50:22+03:00",            //git特有
                "actions": [
                    {
                      "action": "create",
                      "file_path": "folder1/foobar",
                      "content": "some content"
                    },...
                    ],
                "resource": "https://<server>/<cmadapter>/v1/projects/{pname}/commit/ed899a2f4b50b4370feeea94676502b42383c746"
              },
            ]
    }

取某个具体提交信息

#### URI

        http://<server>/<cmadapter>/v1/projects/{pname}/commit/{revision}

 {revision} svn revision, git commit id 

**方法:GET**

返回值 

      {
        "type":"log",                                         //内置参数
        "project": "group1/prj1",                             //内置参数
        “branch”: ”trunk”,                                    //内置参数
        "revision":"ed899a2f4b5",                            //内置参数
        "message": "Replace sanitize with escape once",        //内置参数
        "author": "Example User",                            //内置参数
        "date":"2016-5-6 7:30",                                //内置参数
        "id": "ed899a2f4b50b4370feeea94676502b42383c746",
        "short_id": "ed899a2f4b5",
        "title": "Replace sanitize with escape once",
        "committer_name": "Administrator",
        "committer_email": "admin@example.com",
        "committed_date": "2012-09-20T11:50:22+03:00",
        "created_at": "2012-09-20T11:50:22+03:00",
        "message": "Replace sanitize with escape once",
        "actions": [
            {
              "action": "create",
              "file_path": "foo/bar",
              "content": "some content"
            },...
            ],
        "resource": "https://<server>/<cmadapter>/v1/projects/{pname}/commits/ed899a2f4b50b4370feeea94676502b42383c746"
      }

***

### 项目组资源

----------

项目组资源；  

URI                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            

    http://<server>/<cmadapter>/v1/projectgroups/{gname}

gname 项目组路径，注意与pname的区别是最后一级不会是项目名，而且可以为空，表示根目录

**方法:GET**

获取组中的结构信息，注意会分页

| 参数              | 说明               |
| --------------- |:---------------- |
| return=all (默认) | 缺省值，返回下层项目与组基本信息 |
| return=project  | 仅返回下层项目信息        |
| return=group    | 仅返回下层项目组信息       |

返回值 

    { 
        "type":"groups", 
        "name":"group2", 
        "path":"group1/group2",
        "member": [ 
            {
                "type":"projectgroup", "name":"group3", "resource":"https://xxx/v1/projectgroups/group1/group2/group3", 
            } 
            ,
            {
                "type":"project", "name":"prj1", "resource":"https://xxx/v1/projects/group1/group2/prj1" 
            }            
            ,… 
        ] 
    } 

**方法:POST**

在下层创建项目组或者项目，支持批量创建，原子操作，失败时全部操作不生效

| 参数           | 说明                                    |
| ------------ |:------------------------------------- |
| ignore-exist | 默认False，如果资源已存在，True时忽略继续新建，False报错返回 |

提交JSON字符串 ContentType=”application/json”

如果使用Form方式，ContentType=”application/x-www-form-urlencoded”,同时把JSON字符串放在 param字段中 Request[“param”]

    [
        {type:"project", name:"project2"},
        {type:"projectgroup", name:"group4"},
        ,...
    ]

返回值 

    result_code 返回码，正常可以不用返回，默认值0
    result_message 返回描述，正常可以不用返回，默认值空
    exit_groups, exit_prjects 返回已存在的组与项目列表

***

### 分支资源

----------

分支资源；  

URI                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            

    http://<server>/<cmadapter>/v1/projects/{pname}/branchs

pname 项目路径名

**方法:GET**

获取所有分支信息

| 参数  | 说明  |
| --- |:--- |

返回值 

    [
            {"type":"branch","name":"master","default":true,"revision","abcdefg"},
            {"type":"branch","name":"qabranch","revision","2ad345c3"}
    ]
    
    
    gitlab 参考样例
    [
      {
        "name": "main",
        "merged": false,
        "protected": true,
        "default": true,
        "developers_can_push": false,
        "developers_can_merge": false,
        "can_push": true,
        "commit": {
          "author_email": "john@example.com",
          "author_name": "John Smith",
          "authored_date": "2012-06-27T05:51:39-07:00",
          "committed_date": "2012-06-28T03:44:20-07:00",
          "committer_email": "john@example.com",
          "committer_name": "John Smith",
          "id": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c",
          "short_id": "7b5c3cc",
          "title": "add projects API",
          "message": "add projects API",
          "parent_ids": [
            "4ad91d3c1144c406e50c7b33bae684bd6837faf8"
          ]
        }
      },
      ...
    ]

**方法:POST**

创建分支

| 参数     | 说明               |
| ------ |:---------------- |
| branch | 新建分支名称           |
| from   | 分支名或者修订版，创建分支的源头 |

返回值 

      {
        "name": "newbranch",
        "merged": false,
        "protected": true,
        "default": false,
        "developers_can_push": false,
        "developers_can_merge": false,
        "can_push": true,
        "commit": {
          "author_email": "john@example.com",
          "author_name": "John Smith",
          "authored_date": "2012-06-27T05:51:39-07:00",
          "committed_date": "2012-06-28T03:44:20-07:00",
          "committer_email": "john@example.com",
          "committer_name": "John Smith",
          "id": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c",
          "short_id": "7b5c3cc",
          "title": "add projects API",
          "message": "add projects API",
          "parent_ids": [
            "4ad91d3c1144c406e50c7b33bae684bd6837faf8"
          ]
        }
      }
    
    or
    
    result_code 返回码，正常可以不用返回，默认值0
    result_message 返回描述，正常可以不用返回，默认值空

***

### 标签资源

---

URI
```
http://<server>/<cmadapter>/v1/projects/{pname}/tags
```
- pname 项目路径名

**方法:GET**

获取所有分支信息

| 参数  | 说明  |
| --- |:--- |

返回值
```json
[
    {
      "date":"2020-12-08 06:04:00",
      "committed_date":"2020-12-08 06:04:00",
      "protected":true,
      "author":"abc",
      "name":"new_tag_1",
      "type":"tag",
      "tagMessage":"",
      "message":"tag message",
      "revision":"7b5c3cc8be40ee161ae89a06bba6229da1032a0c"
    }
]
```
**方法:POST**

创建标签

| 参数      | 说明                             |
|---------|:-------------------------------|
| tag     | 新建标签名称                         |
| from    | 参照对象, 可以是已有`分支名`/`标签名`/`提交sha` |
| message | 标签描述信息                         |

返回值
```json
{
    "name": "new_tag",
    "type": "tag",
    "message": "tag message",
    "revision": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c"
}
```
或
```json
{
  "result_code": 0,
  "result_message": ""
}
```
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

**方法:DELETE**

删除标签

| 参数  | 说明       |
|-----|:---------|
| tag | 要删除的标签名称 |

返回值
```json
{
  "result_code": 0,
  "result_message": ""
}
```
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空


## 管理接口

**所有高级方法都需要安全访问授权{AccessToken}** , ~~非用户身份信息，可放参数，也可以~~**放请求头部**，为了安全~~，优先放头部~~

### 项目资源

----------

#### 获取项目信息
版本管理工具中的项目资源,对于SVN工具，项目既仓库 

URI
```
http://<server>/<cmadapter>/v1/projects/{pname}
```
pname 项目路径名，如果项目在目录（分组）之下，则写为: group1/project1； 
为了方便从路径中分解出项目名，可以在路径中增加一个特殊的分段 /-/ 来辅助截断，如： http://server/cmadapter/v1/projects/group1/project1/-/tree/.../... ，但 /-/ 也不是必须的，适配器应兼容不写的方式；

**方法:GET**

返回值 
```json
{
    "type": "project",
    "name": "abc",
    "resource": "<url>",
    "clones": [
        {
            "name": "SVN协议",
            "description": "",
            "protocol": "svn",
            "url": "svn://xxx"
        },
        {
            "name": "HTTPS协议",
            "description": "",
            "protocol": "https",
            "url": "https://xxx"
        },
        ......
    ]
}
```
or 

状态码 404,...

#### 修改项目信息，暂时没有功能
URI
```
http://<server>/<cmadapter>/v1/projects/{pname}
```
**方法:PUT**

提交JSON字符串 ContentType=”application/json”

    {type:"project", name:"abc", ...}

返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

#### 删除项目（仓库），谨慎使用
URI
```
http://<server>/<cmadapter>/v1/projects/{pname}
```
**方法:DELETE**

返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

#### 获取所有项目信息
获取所有项目信息，注意会分页

URI
```
http://<server>/<cmadapter>/v1/projects
```
**方法:GET**


返回值 
```json
[
  {"type":"project","name":"abc","resource":"<url>"},
  ...
] 
```

#### 新建项目
URI
```
http://<server>/<cmadapter>/v1/projects/{pname}
```
**方法:POST**

提交JSON字符串 ContentType=”application/json”

如果使用Form方式，ContentType=”application/x-www-form-urlencoded”,同时把JSON字符串放在 param字段中 Request[“param”]
```json
{
  "type":"project", 
  "name":"abc"
}
```
返回值 
```json
{
  "type":"project", 
  "name":"abc", 
  "resource":"<url>"
}
```
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

#### 批量删除项目
批量删除项目, **暂不支持**

URI
```
http://<server>/<cmadapter>/v1/projects/{pname}
```
**方法:DELETE**

```json
[
  {"type":"project", "name":"abc"}, 
  ...
]
```
返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

### 用户资源

----------

 版本管理工具中的用户资源，**所有高级方法都需要访问授权**   

#### 获取用户信息
URI
```
http://<server>/<cmadapter>/v1/users/{uname}
```
**方法:GET**

| 参数  | 说明  |
| --- |:----|
| uname | 用户名 |

返回值 
```json
{
  "type":"user",
  "name":"abc"
}
```
or 

状态码 404, 500 ...

#### 修改用户信息
修改用户信息，如密码，{uname}不能为空

URI
```
http://<server>/<cmadapter>/v1/users/{uname}
```
**方法:PUT**

提交JSON字符串 ContentType=”application/json”
```json
{
  "type":"user", 
  "name":"abc", 
  "pwd":"password"
}
```
返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

#### 删除单个用户
URI
```
http://<server>/<cmadapter>/v1/users/{uname}
```
**方法:DELETE**

返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

#### 获取用户清单
URI
```
http://<server>/<cmadapter>/v1/users
```
**方法:GET**

返回值 
```json
[
    {"type":"user","name":"abc"},
    ...
]
```
or 

状态码 404,...

#### 新建用户
新建用户,支持批量创建，原子操作，失败时所有操作不生效。

URI
```
http://<server>/<cmadapter>/v1/users
```
**方法:POST**

| 参数           | 说明                                                 |
| ------------ |:-------------------------------------------------- |
| overwrite    | 默认False，如果用户已存在，True时覆盖更新，False 则看 ignore-exist 的值 |
| ignore-exist | 默认False，如果用户已存在，True时忽略继续更新，False报错                |

提交JSON字符串 ContentType=”application/json”

如果使用Form方式，ContentType=”application/x-www-form-urlencoded”,同时把JSON字符串放在`param`字段中 Request[“param”]

```json
[
  {"type":"user", "name":"abc", "pwd":"password", "nickname": "张三"},
  ...
]
```

返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空
- exist_users 返回已存在的用户列表，无论成功失败，新建用户已存在就返回

#### 批量删除用户
**!!!暂未启用!!!**

URI
```
http://<server>/<cmadapter>/v1/users
```
**方法:DELETE**

```json
[
  {"type":"user", "name":"abc"}, 
  ...
]
```
返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

### 项目角色资源

----------

 版本管理工具中的用户组资源,{role-name}角色名；{atoken}为适配器的授权   

#### 获取项目角色信息
URI
```
http://<server>/<cmadapter>/v1/projects/{pname}/roles/{role-name}
```
**方法:GET**

返回值 
```json
{
  "type":"role",
  "name":"abc",
  "members":[
      <uid1>,
      <uid2>,
      ...
  ]
}
```
or 

状态码 404,...

#### 修改用户组信息
URI
```
http://<server>/<cmadapter>/v1/projects/{pname}/roles/{role-name}
```
**方法:PUT**

修改用户组信息，如成员

提交JSON字符串 ContentType=”application/json”
```json
{
  "type":"role", 
  "name":"abc", 
  "members":[...]
}
```
返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

#### 删除用户组
URI
```
http://<server>/<cmadapter>/v1/projects/{pname}/roles/{role-name}
```
**方法:DELETE**

返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

#### 获取项目角色信息
URI
```
http://<server>/<cmadapter>/v1/projects/{pname}/roles
```
**方法:GET**

返回值 
```json
[
  {
    "type":"role",
    "name":"abc",
    "member":[...]
  },
  ...
] 
```
or 

状态码 404,...

#### 新建用户组
新建用户组，支持批量创建，原子操作，失败时全部操作不生效

URI
```
http://<server>/<cmadapter>/v1/projects/{pname}/roles
```
**方法:POST**


| 参数           | 说明                                                  |
| ------------ |:--------------------------------------------------- |
| overwrite    | 默认False，如果用户组已存在，True时覆盖更新，False 则看 ignore-exist 的值 |
| ignore-exist | 默认False，如果用户组已存在，True时忽略继续更新，False报错                |

提交JSON字符串 ContentType=”application/json”

如果使用Form方式，ContentType=”application/x-www-form-urlencoded”,同时把JSON字符串放在 param字段中 Request[“param”]
```json
[
  {"type":"role", "name":"abc", "members":[...]},
  ...
]
```
返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空
- exit_roles 返回已存在的角色列表，如果新建角色已存在，不管overwrite的值

#### 批量删除用户组
URI
```
http://<server>/<cmadapter>/v1/projects/{pname}/roles
```
**方法:DELETE**

```json
[
  {"type":"role", "name":"abc"}, 
  ...
]
```
返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

### 权限管理API

----------

#### 查询权限

{atoken}为适配器的授权   

URI                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
```
http://<server>/<cmadapter>/v1/projects/{pname}/api/query_authority
http://<server>/<cmadapter>/v1/projects/{pname}/-/api/query_authority
```
**方法:GET**

| 参数         | 是否必要 | 说明                                                                                                  |
| ---------- |---|:----------------------------------------------------------------------------------------------------|
| target     | 是 | 目标资源路径，必须指定                                                                                         |
| user       | 否 | 用户，多个中间用【，】分割                                                                                       |
| role       | 否 | 项目角色，多个中间用【，】分割                                                                                     |
| permission | 否 | 查询指定SVN权限, 可选`READ`/`READWRITE`/`NONE`，多个中间可用【，】分割                                                  |
| access_role | 否 | 查询指定GitLab角色权限, GitLab角色权限, 可选 `Guest`/`Reporter`/`Developer`/`Maintainer`，多个中间可用【，】分割 |

查询符合条件的所有授权信息

返回值 
- 查询SVN适配器
```json
[
  {
    "target":"/project1/ci1/folder1",
    "users":[],
    "roles":[],
    "permission":"READ"
  }
]
```
- 查询GitLab适配器
```json
[
  {
    "target":"/project1/ci1/folder1",
    "users":[],
    "roles":[],
    "access_role":"Developer"
  }
]
```
***

#### 设置权限

{atoken}为适配器的授权   

URI                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
```
http://<server>/<cmadapter>/v1/projects/{pname}/api/assign_authority
http://<server>/<cmadapter>/v1/projects/{pname}/-/api/assign_authority
```
**方法:POST**

| 参数          | 是否必要                         | 说明                                                                               |
|-------------|------------------------------|:---------------------------------------------------------------------------------|
| target      | 是                            | 目标资源路径                                                                           |
| user        | 否(user或role必传其一)             | 用户，多个中间用【，】分割                                                                    |
| role        | 否(user或role必传其一)             | 项目角色，多个中间用【，】分割                                                                  |
| permission  | 否(permission或access_role必传其一) | SVN权限, 可选 `READ`/`READWRITE`/`NONE`                                              |
| access_role | 否(permission或access_role必传其一) | GitLab角色权限, 可选 `Guest`/`Reporter`/`Developer`/`Maintainer` |

返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

#### 删除权限

移除权限信息，恢复为无权限或者继承权限；{atoken}为适配器的授权   

URI                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
```
http://<server>/<cmadapter>/v1/projects/{pname}/api/remove_authority
http://<server>/<cmadapter>/v1/projects/{pname}/-/api/remove_authority
```
**方法:POST**

| 参数         | 是否必要             | 说明                                                                                 |
| ---------- |------------------|:-----------------------------------------------------------------------------------|
| target     | 是                | 目标资源路径                                                                             |
| user        | 否(user或role必传其一) | 用户，多个中间用【，】分割                                                                    |
| role        | 否(user或role必传其一) | 项目角色，多个中间用【，】分割                                                                 |
| permission  | 否                | SVN权限, 可选 `READ`/`READWRITE`/`NONE`, 不指定默认删除所有权限                                   |
| access_role | 否                | GitLab角色权限, 可选 `Guest`/`Reporter`/`Developer`/`Maintainer`, 不指定默认删除所有权限 |

返回值 
- result_code 返回码，正常可以不用返回，默认值0
- result_message 返回描述，正常可以不用返回，默认值空

***

# 附录

----------

示例代码

发送请求

    String urltest = "http://192.168.1.133/alm/rest/cm/ci/333";
    
    Uri uri = new Uri(urltest);
    
    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
    
    request.Method = "POST";
    
    //request.ContentType = "application/json;charset=UTF-8";
    
    string authInfo = Convert.ToBase64String(Encoding.Default.GetBytes("Junius:1"));
    
    request.Headers["Authorization"] = "Basic " + authInfo;
    
    string inputData = "{\\"aaa\\":\\"helloworld!\\"}";
    
    string postFormData = "param=" + inputData;
    
    string postData = "{\\"aaa\\":\\"helloworld!\\"}";
    
    UTF8Encoding encoding = new UTF8Encoding();
    
    byte[] byte1 = encoding.GetBytes(postFormData);
    
    // Set the content type of the data being posted.
    
    request.ContentType = "application/x-www-form-urlencoded";
    
    //request.ContentType = "application/json";
    
    // Set the content length of the string being posted.
    
    request.ContentLength = byte1.Length;
    
    Stream newStream = request.GetRequestStream();
    
    newStream.Write(byte1, 0, byte1.Length);
    
    // Close the Stream object.
    
    newStream.Close();
    
    try
    
    {
    
    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    
    //Stream myResponseStream = response.GetResponseStream();
    
    //StreamReader myStreamReader = new StreamReader(myResponseStream,
    Encoding.GetEncoding(encoding));
    
    //retString = myStreamReader.ReadToEnd();
    
    //myStreamReader.Close();
    
    //myResponseStream.Close();
    
    }
    
    catch (WebException ex)
    
    {
    
    byte[] contentBytes = new byte[ex.Response.ContentLength];
    
    ex.Response.GetResponseStream().Read(contentBytes,0,(int)ex.Response.ContentLength);
    
    string content = encoding.GetString(contentBytes);
    
    }

处理请求

    string contentType = request.ContentType;
    
    int idx = contentType.IndexOf(";");
    
    contentType = (idx == -1) ? contentType : contentType.Substring(0, idx);
    
    string content;
    
    if (contentType == "application/x-www-form-urlencoded")
    
    {
    
    content = request.Form["param"];
    
    }
    
    else
    
    {
    
    byte[] contentBytes = new byte[request.ContentLength];
    
    request.InputStream.Read(contentBytes, 0, request.ContentLength);
    
    content = request.ContentEncoding.GetString(contentBytes);
    
    }
    
    List\<Dictionary\<string, string\>\> pData = null;
    
    try
    
    {
    
    pData = JsonConvert.DeserializeObject\<List\<Dictionary\<string,
    string\>\>\>(content);
    
    }
    
    catch (System.Exception ex)
    
    {
    
    throw new HttpException(400, ex.Message);
    
    }

## ChangeLog

| 版本   | 更新          |
|:---- |:----------- |
| v1.0 | 重新定义适配器高级接口 |
