# 记录活动RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 添加记录活动

#### 接口地址：

    http://<server>/alm/rest/rusem/RueseInfo/{pid}/AddActivityLog?
    nodeId={nodeId}&UToken={utoken}

#### 请求方式：

    post

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明    |
| ------ | ------ | ---- | ----- |
| pid    | string | 是    | 项目id  |
| nodeId | string | 是    | 操作单id |
| UToken | string | 是    | 用户令牌  |

#### 输出结果：

    返回string(字典)

## 根据重用件和版本号为操作单依赖列表赋值

#### 接口地址：

```
http://<server>/alm/rest/rusem/RueseInfo/{reuseInfoId}/
{versionId}/AddDependenceRIList?proId={pid}&nodeId={nodeId}
&UToken={utoken}
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数          | 类型     | 是否必填 | 说明      |
| ----------- | ------ | ---- | ------- |
| reuseInfoId | string | 是    | 重用件Uid  |
| versionId   | string | 是    | 版本号     |
| pid         | string | 是    | 项目id    |
| nodeId      | string | 是    | 当前操作单id |
| UToken      | string | 是    | 用户令牌    |

#### 输出结果：

    返回string(字典属性)
