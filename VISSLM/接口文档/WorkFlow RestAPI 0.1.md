# 工作流RestAPI接口说明

此接口为VISSLM提供, 接口实现形式为 HTTP Restful

\*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

    http://location:port/path?param=value#flag

## 获取流程操作信息

---------

#### URI

    http://{<server>}/alm/rest/WorkFlow/GetFlowOperationList?flowid={flowid}
    &UToken={Utoken}

#### 方法

    Get

#### 参数

| 参数     | **必选** | **类型** | 说明    |
| ------ |:------ | ------ | ----- |
| flowid | 是      | string | 流程ID  |
| UToken | 是      | string | token |

#### 返回值

    {
        "ErrorCode": 0,
        "ErrorMsg": "添加成功",
        "Data": [
            {
                "NodeName": "确认中",
                "StartTime": "2020/01/14 19:51:00",
                "EndTime": "2020/01/14 19:51:00",
                "DiffTimes": "1分10秒",
                "Executor": "user",
                "ExecutorName": "测试用户",
                "OperateName": "确认"
            }
        ]
    }

#### 返回Code

| 参数    | 说明       |
| ----- |:-------- |
| 0     | 表示添加成功   |
| -10   | 错误(查看消息) |
| 30000 | 字段不能为空   |
| 30001 | 字段数据类型错误 |

#### 返回参数Data说明

| 参数           | 类型     | 说明    |
| ------------ | ------ | ----- |
| NodeName     | string | 流程节点  |
| StartTime    | string | 进入时间  |
| EndTime      | string | 流出时间  |
| DiffTimes    | string | 持续时间  |
| Executor     | string | 执行人   |
| ExecutorName | string | 执行人名称 |
| OperateName  | string | 操作节点  |

***

## 根据对象ID获取流程列表

---------

#### URI

    http://{<server>}/alm/rest/WorkFlow/GetWorkFlowListByUid?uid={uid}
    &UToken={Utoken}&state={state}

#### 方法

    Get

#### 参数

| 参数                                                       | **必选** | **类型** | 说明                                                        |
| -------------------------------------------------------- |:------ | ------ | --------------------------------------------------------- |
| uid                                                      | 是      | string | 对象ID                                                      |
| UToken                                                   | 是      | string | token                                                     |
| state                                                    | 否      | string | 流程状态：不传值默认为Started；也可传多个值用逗号隔开如：Started,Finished,Canceled |
| ***注【state】包含三种状态：开启【Started】；完成【Finished】;取消【Canceled】; |        |        |                                                           |

#### 返回值

    {
        "ErrorCode": 0,
        "ErrorMsg": "添加成功",
        "Data": [
            {
                "FlowID": "aaaaa-aaaaa-aaa",
                "FlowName": "测试流程",
                "State": "Started",
                "StateName": "工作流启动",
                                "CreateTime":"2022-01-01 10:10:10"
            }
        ]
    }

#### 返回Code

| 参数    | 说明       |
| ----- |:-------- |
| 0     | 表示添加成功   |
| -10   | 错误(查看消息) |
| 30000 | 字段不能为空   |
| 30001 | 字段数据类型错误 |

#### 返回参数Data说明

| 参数         | 类型     | 说明   |
| ---------- | ------ | ---- |
| FlowID     | string | 流程ID |
| FlowName   | string | 流程名称 |
| State      | string | 状态码  |
| StateName  | string | 状态名称 |
| CreateTime | string | 创建时间 |

***

## 流程统计

#### 接口地址：

```
http://<server>/alm/rest/workflow/GetFlowStatisticsList?flowid=
{flowid}&UToken={UToken}&state={state}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| flowid | string | 是    | 流程id |
| state  | string | 是    | 流程状态 |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    返回JSON格式"[]"，包括流