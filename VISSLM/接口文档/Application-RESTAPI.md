# 数据库配置管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 获取VISSLM数据库版本信息

#### 接口地址：

    http://<server>/alm/rest/application/DBVersion

#### 请求方式：

    get

#### 输入参数：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    "DBVersion"

## 获取VISSLM版本信息

#### 接口地址：

    http://<server>/alm/rest/application/Version

#### 请求方式：

    get

#### 输入参数：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    "AppVersion"
