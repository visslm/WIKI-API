# 邮件RestApi说明

## 一、发送指定邮件内容的邮件

#### 场景说明：

    发送指定邮件内容的邮件

#### 接口地址：

    url：http://<server>/alm/rest/api/SendSPMail

#### 请求方式：

```
Post
```

#### 参数输入：

    parameter（string），必填，邮件基本信息Json字符串，Key值说明如下：
        SendTo（string），收件人地址，多个地址逗号分隔；
        SendCc（string），抄送人地址，多个地址逗号分隔；
        SendBcc（string），密送人地址，多个地址逗号分隔；
        Subject（string），邮件标题；
        Body（string），邮件正文。

#### 结果输出：

    对象，具体Key，Value说明如下：
    ErrorCode：0（成功）/-1（失败）
    ErrorMsg：成功(0)/失败(-1)
    Data：bool（true/false）

## 二、发送邮件

#### 场景说明：

    发送指定邮件模板和收件信息(项目内用户)的邮件

#### 接口地址：

    url: http://<server>/alm/rest/api/SendMail

#### 请求方式：

    Post

#### 参数输入：

| 参数名          | 类型     | 是否必填 | 说明                                    |
| ------------ | ------ | ---- | ------------------------------------- |
| entityId     | string | 必填   | 条目Id                                  |
| emailTmpName | string | 必填   | 邮件模板名称                                |
| sendTo       | string | 必填   | 收件人，多个用户Id用逗号","分割                    |
| copyTo       | string | 可选   | 抄送人，多个用户id用逗号","分割                    |
| mateData     | string | 可选   | 上下文数据（格式：JSON字符串{"_valm_Uid": "123"}) |
| utoken       | string | 必填   | 当前用户令牌utoken                          |

#### 输出结果：

    对象，具体Key，Value说明如下：
    ErrorCode：0（成功）/-1（失败）
    ErrorMsg：成功(0)/失败(-1)
    Data：bool（true/false）
    
    返回：{"ErrorCode":0,"ErrorMsg":"成功","Data":true}
