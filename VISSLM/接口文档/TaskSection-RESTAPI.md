# 工作区任务管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 通过任务ID获取工作区任务数据

#### 接口地址：

    http://<server>/alm/rest/TaskSection/GetWorkDataByID?UToken={utoken}
    &taskID={taskID}&secionDate={sectionDate}&executor={executor}

#### 请求方式：

    get

#### 参数输入：

| 参数          | 类型     | 是否必填 | 说明   |
| ----------- | ------ | ---- | ---- |
| taskID      | string | 是    | 任务ID |
| sectionDate | string | 是    | 日期   |
| executor    | string | 是    | 用户名  |
| UToken      | string | 是    | 用户令牌 |

#### 输出结果：

    {"ErrorCode":"0","ErrorMsg":"成功","Date":[{},{}]}

## 添加工作区任务数据

#### 接口地址：

```
http://<server>/alm/rest/TaskSection/AddTaskSection?UToken=
{utoken}&taskId={taskId}&secionDate={sectionDate}&executor={executor}
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数          | 类型                            | 是否必填 | 说明       |
| ----------- | ----------------------------- | ---- | -------- |
| taskID      | string                        | 是    | 任务ID     |
| sectionDate | string                        | 是    | 日期       |
| executor    | string                        | 是    | 用户名      |
| UToken      | string                        | 是    | 用户令牌     |
| dataArr     | JSONDictionary<string,string> | 是    | 数据属性及属性值 |

#### 输出结果：

    {"ErrorCode":"0","ErrorMsg":"成功","Data":"True"}
