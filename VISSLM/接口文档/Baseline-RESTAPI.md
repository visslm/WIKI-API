# 基线管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 自动创建基线申请单

#### 接口地址：

    http://<server>/alm/rest/api/CreateBaselineRequest

#### 请求方式：

    Post

#### 参数输入：

| 参数     | 类型                            | 是否必填 | 说明         |
| ------ | ----------------------------- | ---- | ---------- |
| blcfg  | string                        | 是    | 基线配置Uid    |
| UToken | string                        | 是    | 用户令牌       |
| param  | JSONDictionary<string,string> | 否    | 基线申请单的基本信息 |

#### 输出结果：

    {"xxxx":"rrrrr","ResultMsg":"True"}

## 自动创建基线(自动检索基线配置，如无则自动创建)

#### 接口地址：

    http://<server>/alm/rest/api/CreateGeneralBaseLine

#### 请求方式：

    Post

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明           |
| ------ | ------ | ---- | ------------ |
| nodeId | string | 是    | 将要创建基线的对象uid |
| UToken | string | 是    | 用户令牌         |
| param  | string | 否    | 基线对象的基本信息    |

#### 输出结果

    {"xxxx":"rrrrr","ResultMsg":"True"}

## 自动备份基线数据

#### 接口地址：

    http://<server>/alm/rest/api/CreateGeneralBaseLineData

#### 请求方式：

    Post

#### 参数输入：

| 参数      | 类型     | 是否必填 | 说明                       |
| ------- | ------ | ---- | ------------------------ |
| proId   | string | 是    | 项目id                     |
| uids    | string | 是    | 基线包含配置项uid集(多个uid用“,”分割) |
| baseUid | string | 是    | 基线对象uid                  |
| UToken  | string | 是    | 用户令牌                     |

#### 输出结果：

    {"xxxx":"rrrrr","ResultMsg":"True"}

## 验证基线uid创建包含配置项历史数据

#### 接口地址：

    http://<server>/alm/rest/api/VerifyCBLSubBaselineCfg

#### 请求方式：

    Post

#### 输入参数：

| 参数          | 类型                            | 是否必填 | 说明         |
| ----------- | ----------------------------- | ---- | ---------- |
| relateblcfg | string                        | 是    | 包含子基线配置uid |
| UToken      | string                        | 是    | 用户令牌       |
| param       | JSONDictionary<string,string> | 否    | 条目或上下文信息   |

#### 输出结果：

    "{\"msgRes\":\"true\",\"msgTxt\":\"resultmsg\"}"

## 复合基线中包含子基线的验证

#### 接口地址：

    http://<server>/alm/rest/api/VerifyCBLSubBaseline

#### 请求方式：

    Post

#### 输入参数：

| 参数     | 类型             | 是否必填 | 说明    |
| ------ | -------------- | ---- | ----- |
| subbl  | string         | 是    | 包含子基线 |
| UToken | string         | 是    | 用户令牌  |
| param  | JSONDictionary | 否    |       |

#### 输出结果:

    "{\"msgRes\":\"True\",\"msgTxt\":\"resultmsg\"}"

## 验证配置项和基线配置是否存在重复的配置项或基线配置

#### 接口地址：

    http://<server>/alm/rest/api/VerifyCTBaselineCMI

#### 请求方式：

    Post

#### 输入参数：

| 参数     | 类型                             | 是否必填 | 说明                                                                   |
| ------ | ------------------------------ | ---- | -------------------------------------------------------------------- |
| UToken | string                         | 是    | 用户令牌                                                                 |
| param  | JSONDictionary<string, string> | 否    | 如：{"CIs":"包含配置项的ID(1,2...)","RelateBaselineCfg":"包含子基线配置ID(1,2...)"} |

#### 输出结果：

    {"ErrorCode":"0","ErrorMessage":"","Data",""}
