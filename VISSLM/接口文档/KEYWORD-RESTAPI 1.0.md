# 对象关键字RestAPI接口说明

此接口为VISSLM提供, 接口实现形式为 HTTP Restful

\*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

    http://location:port/path?param=value#flag

## 通用参数

所有接口的通用参数

LoginUser：用户信息

| 参数           | 说明        |
| ------------ | --------- |
| AppName      | 访问服务的应用名称 |
| UToken       | 用户登陆后令牌   |
| UserName     | 用户登录名     |
| UserPassword | 用户密码      |

> UToken与(UserName、UserPassword)二选一，同时存在时使用用户名与密码验证

获取项目内关键字统计情况
---------

#### URI

      http://<server>/alm/rest/keyword/GetKeyWordStatistics?proId={proId}
      &UToken={utoken}

#### 方法

    GET

#### 参数

| 参数     | 类型     | 是否必填 | 说明   |
| ------ |:------ |:---- |:---- |
| proId  | string | 是    | 项目id |
| UToken | string | 是    | 用户令牌 |

#### 返回值

   {"ErrorCode":0,"ErrorMsg":"成功","Data":[{"KeyText":"tag","Count":1}]}

***
