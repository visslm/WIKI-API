# 过程性能基线数据RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 获取历史项目数据

#### 接口地址：

    http://<server>/alm/rest/ppb/GetHistoryProjectData?
    historyInfoUid={historyInfoUid}&itemId={itemId}&UToken={utoken}

#### 请求方式：

    get

#### 参数输入：

| 参数             | 类型     | 是否必填 | 说明      |
| -------------- | ------ | ---- | ------- |
| historyInfoUid | string | 是    | 历史项目Uid |
| itemId         | string | 否    | 测量项编号   |
| UToken         | string | 是    | 用户令牌    |

#### 输出结果：

    返回历史项目数据 (HistoryProjectData封装数据)

## 获取过程性能基线数据

#### 接口地址：

```
http://<server>/alm/rest/ppb/GetPPBData?ppbUid={ppbUid}&itemId={itemId}&UToken={utoken}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明       |
| ------ | ------ | ---- | -------- |
| ppbUid | string | 是    | 过程性能基线id |
| itemId | string | 否    | 测量项编号    |
| UToken | string | 是    | 用户令牌     |

#### 输出结果：

    返回过程性能基线数据 (PPBData封装数据)
