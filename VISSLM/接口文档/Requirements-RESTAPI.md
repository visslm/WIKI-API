# 需求文档管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 获取文档内容

#### 接口地址：

    http://<server>/alm/rest/requirements/document/{id}?
    ReturnProperty={retProps}&baselineId={baselineId}

#### 请求方式：

    get

#### 参数输入：

| 参数             | 类型     | 是否必填 | 说明     |
| -------------- | ------ | ---- | ------ |
| id             | string | 是    | 文档id   |
| ReturnProperty | sring  | 是    | 返回属性   |
| baselineId     | string | 是    | 基线Id   |
| no-text        | string | 是    | 文档样式属性 |
| with-content   | string | 是    | 文档样式属性 |
| with-enumname  | string | 是    | 文档样式属性 |
| with-inlink    | string | 是    | 文档样式属性 |
| with-outlink   | string | 是    | 文档样式属性 |
| set-traceid    | string | 是    | 文档样式属性 |
| page           | string | 是    | 页码     |
| rows           | string | 是    | 页行数    |

#### 输出结果：

    返回JSON格式，{"ErrorCode":0,"ErrorMessage":"","Data": {}}

## 获取文档基线

#### 接口地址：

```
http://<server>/alm/rest/requirements/document/{id}/baselines
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数  | 类型     | 是否必填 | 说明   |
| --- | ------ | ---- | ---- |
| id  | string | 是    | 文档id |

#### 输出结果：

    返回JSON格式，{"ErrorCode":0,"ErrorMessage":"","Data":  [{},{}]}
