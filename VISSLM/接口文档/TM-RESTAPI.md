# 测试管理(适配器)RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 添加代码模型和函数

#### 接口地址：

    http://<server>/alm/rest/tm/FileModule/{pid}/FMAnalyse?UToken={utoken}

#### 请求方式：

    put

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| pid    | string | 是    | 项目id |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    返回JSON格式，{"IsSuccess":"true", "RetMessage":"成功!"}

## 添加测试周期和测试运行数据

#### 接口地址：

```
http://<server>/alm/rest/tm/TCycle/{pid}/{tplanid}/AddTestCycle?UToken={utoken}
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数            | 类型            | 是否必填 | 说明                                                                                                                                                                            |
| ------------- | ------------- | ---- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| pid           | string        | 是    | 项目id                                                                                                                                                                          |
| tplanid       | string        | 是    | 测试计划id                                                                                                                                                                        |
| UToken        | string        | 是    | 用户令牌                                                                                                                                                                          |
| testCycleInfo | TestCycleInfo | 是    | 测试周期信息，包括属性：testCycleProperty JSONDictionary<string,string> 测试周期的属性数据，testSequenceCase List<JSONDictionary<string, string>> 测试序列及测试用例数据，获取默认分组测试用例数据，baseLineName string 基线名称 |

#### 输出结果：

    返回JSON格式，{"IsSuccess":"true", "RetMessage":"成功!","TestCycleUid":"xxx"}

## 设置测试状态

#### 接口地址：

```
http://<server>/alm/rest/tm/TestCycle/{tcid}/TStatus?UToken={utoken}
```

#### 请求方式：

```
get/post
```

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明     |
| ------ | ------ | ---- | ------ |
| tcid   | string | 是    | 测试周期id |
| UToken | string | 是    | 用户令牌   |

#### 输出结果：

    返回"ReturnMsg"

## 设置测试运行的结果

#### 接口地址：

```
http://<server>/alm/rest/tm/TestRun?UToken={utoken}
```

#### 请求方式：

```
put
```

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    返回"True/False"
