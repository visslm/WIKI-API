# Word管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## HTML字符转word

#### 接口地址：

    http://<server>/alm/rest/word/HtmlStrToWord?UToken={utoken}

#### 请求方式：

    post

#### 参数输入：

| 参数     | 类型                            | 是否必填 | 说明   |
| ------ | ----------------------------- | ---- | ---- |
| UToken | string                        | 是    | 用户令牌 |
| param  | JSONDictionary<string,string> | 否    |      |

#### 输出结果：

     {"ErrorCode":"0","ErrorMsg":"操作成功","Data":"xxxfile"}

## HTML转word

#### 接口地址：

```
http://<server>/alm/rest/word/HtmlToWord?UToken={utoken}
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数     | 类型                            | 是否必填 | 说明   |
| ------ | ----------------------------- | ---- | ---- |
| UToken | string                        | 是    | 用户令牌 |
| param  | JSONDictionary<string,string> | 否    |      |

#### 输出结果：

    {"ErrorCode":"0","ErrorMsg":"操作成功","Data":"xxxfile"}

## url转word

#### 接口地址：

```
http://<server>/alm/rest/word/UrlToWord?UToken={utoken}
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数     | 类型                            | 是否必填 | 说明   |
| ------ | ----------------------------- | ---- | ---- |
| UToken | string                        | 是    | 用户令牌 |
| param  | JSONDictionary<string,string> | 否    |      |

#### 输出结果：

    {"ErrorCode":"0","ErrorMsg":"操作成功","Data":"xxxfile"}
