# WebServices接口说明文档 v2.0

此接口为VISSLM提供, 接口实现形式为 WebService，使用SOAP标准消息协议通讯，用WSDL文件进行说明，并通过UDDI进行注册

WebService地址

    http://location:port/almws/VALMService.svc

## 通用参数

所有接口的通用参数

LoginUser：用户信息

| 参数           | 说明        |
| ------------ | --------- |
| AppName      | 访问服务的应用名称 |
| UToken       | 用户登陆后令牌   |
| UserName     | 用户登录名     |
| UserPassword | 用户密码      |

    UToken与(UserName、UserPassword)二选一，同时存在时使用用户名与密码验证

 查询条件【SearchCondition】

| 参数          | 说明    | 样例                                  |
| ----------- | ----- | ----------------------------------- |
| property    | 对象属性  | ProjectStatus,ProjectName,_valm_Uid |
| condition   | 比较运算符 | >、<、=、like,in                       |
| PropertyVal | 属性值   | 1                                   |
| conector    | 连接符   | And或者Or                             |

 排序条件

| 参数          | 说明        | 样例         |
| ----------- | --------- | ---------- |
| property    | [OrderBy] |            |
| condition   | 排序条件      | asc或者desc  |
| PropertyVal | 属性值       | _valm_Sort |
| conector    | 空白        | 空白         |

分页大小条件

| 参数          | 说明         | 样例         |
| ----------- | ---------- | ---------- |
| property    | [PageSize] | [PageSize] |
| condition   | 空白         | 空白         |
| PropertyVal | 每页条数       | 例如：100     |
| conector    | 空白         | 空白         |

页数条件

| 参数          | 说明          | 样例  |
| ----------- | ----------- | --- |
| property    | [PageIndex] |     |
| condition   | 空白          |     |
| PropertyVal | 页数          | 默认1 |
| conector    | 空白          | 空白  |

## 记录操作记录【AddHistoryRecord】

#### 参数说明

| 参数       | 类型                          | 说明   |
| -------- | --------------------------- | ---- |
| pros     | Dictionary<string, dynamic> | 属性值  |
| userInfo | LoginUser                   | 用户信息 |

#### 返回说明

```
返回执行信息，操作记录Uid {"ReturnMsg":"success","Uid":"xxx"}
```

#### 示例

    输入数据：
    pros: {"DataUid":"213265","ChangePros":
    "{\"_valm)Name\": {\"OldVal\":\"r1\",\"NewVal\":\"r1\"}}", 
    "ChangeMsg":"创建对象","ChangeType":"Add", "ProId":"4521",
    "CreateBy":"user","CreateDate":"2020-12-01","NodeType":"Document"}
    userInfo: LoginUser-UToken("TSDLXXXXXXXXXXXXXXXXXXXXXXX")
    
    -------------------------------------------------------------------------
    输出结果：
    {"Uid":"212342","ReturnMsg":"success"}

## 新建Task【AddTask】

#### 参数说明

| 参数           | 类型                         | 说明      |
| ------------ | -------------------------- | ------- |
| propertyList | Dictionary<string, string> | 属性值集合   |
| relatedUid   | int                        | 关联的数据id |
| userInfo     | LoginUser                  | 用户信息    |

#### 返回说明

```
返回数据ID
```

#### 示例

```
输入数据：
propertyList: {"_valm_Name","测试新任务1"}
relatedUid: 213422
userInfo: 用户信息LoginUser(UserName:user,UserPassword:123)

-------------------------------------------------------------------------
输出结果：
{"Uid":"212342","ReturnMsg":"success"}
```

## 上传文件、批量添加附件【BatchAddAttachment】

#### 参数说明

| 参数                 | 类型                   | 说明   |
| ------------------ | -------------------- | ---- |
| attachmentProperty | List<AttachmentInfo> | 附件列表 |
| userInfo           | LoginUser            | 用户信息 |

#### 返回说明

```
返回文件上传后信息 [{"ReturnMsg":"success","ErrorMsg":"操作成功"},{}]
```

#### 示例

    输入数据：
    attachmentProperty：[{Name":"sssx","Description":"",
    "FileName":"xxx.zip","DownloadLink":"","Compression":false}]
    userInfo: 用户信息LoginUser(UserName:user,UserPassword:123)
    
    -------------------------------------------------------------------------
    输出结果：
    [{"Uid":"32134","Name":"sssx","Description":"",
    "FileName":"xxx.zip","DownloadLink":"","Compression":"false",
    "ReturnMsg","success"}]

## 批量上传文件【BatchUploadFile】

#### 参数说明

| 参数         | 类型                         | 说明   |
| ---------- | -------------------------- | ---- |
| files      | Dictionary<string, byte[]> | 文件列表 |
| properties | Dictionary<string, string> | 参数信息 |
| userInfo   | LoginUser                  | 用户信息 |

#### 返回说明

```
返回执行信息 {"ReturnMsg":"success","ErrorMsg":"操作成功","":""}
```

#### 示例

    输入数据：
    files： {"": sdasdasDDDsss==, "222.txt": ZnNmcsdsfscscdsdfds==}
    properties:{"KeepTime":"10000000000"}
    userInfo: 用户信息LoginUser(UserName:user,UserPassword:123)
    
    -------------------------------------------------------------------------
    输出结果：
    {"ReturnMsg":"数据流长度必须大于0，文件名不能为空"}

## 配置管理工具集成接口（开发库）【CMCheckInNotify】

#### 参数说明

| 参数          | 类型        | 说明   |
| ----------- | --------- | ---- |
| url         | string    |      |
| userVersion | string    |      |
| revision    | string    |      |
| userInfo    | LoginUser | 用户信息 |

#### 返回说明

```
返回执行信息 {"ReturnCode":"0","ReturnMessage":"Success"}
```

#### 示例

    输入数据：
    url："http://172.1.1.100:81/svn/Dynamic/1215335/"
    userVersion:""
    revision:""
    userInfo: 用户信息LoginUser(UserName:user,UserPassword:123)
    
    -------------------------------------------------------------------------
    输出结果：
    {"ReturnCode":"0","ReturnMessage":"Success"}

## 转移验证【CheckConditionStr】

#### 参数说明

| 参数           | 类型        | 说明     |
| ------------ | --------- | ------ |
| conditionStr | string    | 条件字符串  |
| entityid     | string    | 对象ID   |
| userInfo     | LoginUser | 用户信息   |
| c_userInfo   | LoginUser | 当前用户信息 |

#### 返回说明

```
返回验证信息 ("true"/"验证失败")
```

#### 示例

    输入数据：
    conditionStr："{\"ObjType\":\"Test1322\",\"OrderBy\":[],\"Conditions\":[
    {\"Conector\":\"\",\"Property\":\"_valm_Name\",\"DataType\":
    \"SINGLELINETEXT\",\"EnumId\":\"\",\"Condition\":\"13\",\"PropertyVal\":
    \"完成\",\"Group\":[]}]}"
    entityid：1250921
    userInfo:用户信息LoginUser(UserName:system,UserPassword:sys)
    c_userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    ------------------------------------------------------------------------
    输出结果：
    "验证失败"

## 关闭Task【CloseTask】

#### 参数说明

| 参数       | 类型        | 说明   |
| -------- | --------- | ---- |
| taskId   | int       | 任务Id |
| userInfo | LoginUser | 用户信息 |

#### 返回说明

```
无返回值
```

#### 示例

    输入数据：
    taskId:877321
    userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    
    -------------------------------------------------------------------------
    输出结果：无

## 删除文件【DelFile】

#### 参数说明

| 参数       | 类型        | 说明       |
| -------- | --------- | -------- |
| fileLink | string    | filecode |
| userInfo | LoginUser | 用户信息     |

#### 返回说明

```
返回执行信息 {"ReturnMsg":"success","ErrorMsg":""}
```

#### 示例

    输入数据：
    fileLink："1e46e95b4a994b4e"
    userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    
    -------------------------------------------------------------------------
    输出结果：
    {"ReturnMsg":"failed","ErrorMsg":"您要删除的文件不存在！"}

## 上传现有的签署文件，没有签署单【FMUploadSignFile】

#### 参数说明

| 参数           | 类型        | 说明       |
| ------------ | --------- | -------- |
| versionUid   | int       | 版本数据Uid  |
| fileUid      | string    | 文件/文档Uid |
| projectUid   | string    | 项目Uid    |
| physicalPath | string    | 文件的物理路径  |
| userInfo     | LoginUser | 用户信息     |

#### 返回说明

```
返回执行信息 {"ReturnMsg":"success/failed","ErrorMsg":"xxx"}
```

#### 示例

    输入数据：
    versionUid：1333
    fileUid：78294
    projectUid：10826
    physicalPath："20171103\Project\Attachment\bfdcsffkh90s4d564f"
    userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    
    ------------------------------------------------------------------------
    输出结果：
     {"ReturnMsg":"failed","ErrorMsg":"签署文档版本生成失败！"}

## Python格式化表达式【FormatExpression】

#### 参数说明

| 参数         | 类型                        | 说明     |
| ---------- | ------------------------- | ------ |
| expression | string                    | 表达式字符串 |
| entityId   | string                    | 对象Id   |
| entityType | string                    | 对象类型   |
| mateData   | Dictionary<string,string> | 元数据    |

#### 返回说明

```
返回格式化以后的表达式
```

#### 示例

    输入数据：
    expression："2020-10-10"
    entityId：32152
    entityType："Task"
    mateData：{"_valm_CreateTime":"2017/10/10 11:26"}
    
    -------------------------------------------------------------------------
    输出结果：
    "2020-10-10"

## 根据文件Code获取文件数据【GetFileByCode】

#### 参数说明

| 参数             | 类型     | 说明            |
| -------------- | ------ | ------------- |
| fileUrl        | string | filecode      |
| isNeedFileData | bool   | 是否需要文件的Data数据 |

#### 返回说明

```
返回文件数据 (GetFileByCodeServiceResponse)
```

#### 示例

    输con入数据：
    fileUrl："4e9ab717309aaaa7"
    isNeedFileData:false
    -----------------------------------------------------------------------
    输出数据：
    Response:
    
    contentType:""
    fileData:true
    finename:""

## 获取IM CallBackUrl【GetIMCallBackUrl】

#### 参数说明

| 参数  | 类型  | 说明  |
| --- | --- | --- |
|     |     |     |

#### 返回说明

```
返回执行信息
```

#### 示例

    输入数据：
    无
    
    -------------------------------------------------------------------------
    输出结果：
    ["http://172.1.1.22/alm/Push/SendIM"]

## 根据对象Id查询当前流程操作【GetOpreates】

#### 参数说明

| 参数       | 类型        | 说明      |
| -------- | --------- | ------- |
| userId   | string    | 用户编号    |
| entityId | string    | 对象Id    |
| userInfo | LoginUser | 用户信息    |
| relate   | bool      | 是否是任务数据 |

#### 返回说明

```
返回操作信息 []   
(FlowActivity集合,包括Name，RelateID，View，ExecValue)
```

#### 示例

    输入数据：
    userId："user"
    entityId：42933
    userInfo：用户信息LoginUser(UserName:user,UserPassword:123)
    relate：true
    
    ------------------------------------------------------------------------
    输出结果：
    []

## 获取角色用户【GetRoleUserAll】

#### 参数说明

| 参数       | 类型        | 说明   |
| -------- | --------- | ---- |
| roleUid  | int       | 角色id |
| nodeUid  | int       | 节点Id |
| userInfo | LoginUser | 用户信息 |

#### 返回说明

```
返回用户信息列表 [] 
(LoginUser集合，包括Uid，Sex，Phone,NickName,Mailbox,Address)
```

#### 示例

    输入数据：
    roleUid：116
    nodeUid：33312
    userInfo：用户信息LoginUser(UserName:user,UserPassword:123)
    -----------------------------------------------------------------------
    输出结果：
    [] 

## 依据条件获取部门中用户列表【GetUserByProjectDept】

#### 参数说明

| 参数         | 类型        | 说明               |
| ---------- | --------- | ---------------- |
| deptmaster | string    | 部门负责人枚举类型，多值逗号分隔 |
| nodeUid    | string    | 对象id             |
| userInfo   | LoginUser | 用户信息             |

#### 返回说明

```
返回用户列表信息 []
(LoginUser集合，包括Uid，Sex，Phone,NickName,Mailbox,Address)
```

#### 示例

    输入数据：
    deptmaster:"12,13,14"
    nodeUid: "33322"
    userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    ------------------------------------------------------------------------
    输出结果：
    [] 

## 根据用户组获取用户【GetUserGroupToUserList】

#### 参数说明

| 参数           | 类型  | 说明    |
| ------------ | --- | ----- |
| userGroupUid | int | 用户组Id |

#### 返回说明

```
返回用户列表信息[]
(LoginUser集合，包括Uid，Sex，Phone,NickName,Mailbox,Address)
```

#### 示例

    输入数据：
    userGroupUid：121
    ------------------------------------------------------------------------
    输出结果：
    []

## 新增IM信息【InsertMessageToDB】

#### 参数说明

| 参数         | 类型     | 说明      |
| ---------- | ------ | ------- |
| messageObj | string | IM的基本信息 |

#### 返回说明

```
返回执行信息 {"ReturnMsg":"success","ErrorMsg":""}
```

#### 示例

    输入数据：
    messageObj: "{\"Subject\":\“ss\”, \"Category\":\"xxxc\",
    \"Content\":\"sssc\",\"Weight\":\"sccsa\",\"Status\":\"\”,
    \"Sender\":\"user\",\"Origin\":\"sss\",
    \"UserNameList\":[\"user\"]}"
    
    ----------------------------------------------------------------------
    输出结果：
    {"ReturnMsg":"failed","ErrorMsg":"写入消息接收人失败"}

## 初始化system token【InternalInitialize】

此接口方法体主要逻辑屏蔽，始终返回fail，建议停用

#### 参数说明

| 参数  | 类型  | 说明  |
| --- | --- | --- |
|     |     |     |

#### 返回说明

```
返回"fail", 此接口目前内容不全
```

#### 示例

    输入数据：
    无
    
    -----------------------------------------------------------------------
    输出结果：
    "fail"

## 根据父节点ID获取子节点【ListContents】

#### 参数说明

| 参数       | 类型        | 说明    |
| -------- | --------- | ----- |
| parentId | string    | 父节点ID |
| userInfo | LoginUser | 用户信息  |

#### 返回说明

```
返回子节点列表信息
```

#### 示例

    输入数据：
    parentId："129763"
    userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    -----------------------------------------------------------------------
    输出结果：
    [{"_valm_Uid":"22121","_valm_ItemID":"TS_SND_cc1","_valm_Name":"Task1",
    "_valm_NodeType":"Task","_valm_Sort":"AAIIIII1F"}]

## 修改对象关联关系【ModifyNodeRelationship】

#### 参数说明

| 参数           | 类型        | 说明     |
| ------------ | --------- | ------ |
| upstreamId   | string    | 上游对象ID |
| downstreamId | string    | 下游对象ID |
| relationship | string    | 关系字段   |
| bAdd         | bool      | 是否新增   |
| userInfo     | LoginUser | 登录用户   |

#### 返回说明

```
返回执行信息 {"ReturnMsg":"success","ErrorMsg":""}
```

#### 示例

    输入数据：
    upstreamId："213215"
    downstreamId："321235"
    relationship："_valm_Relate_to"
    bAdd：true
    userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    
    ---------------------------------------------------------------------
    输出结果：
    {"ReturnMsg":"success"}

## 批量添加对象【MultipleAddNode】

#### 参数说明

| 参数           | 类型                               | 说明        |
| ------------ | -------------------------------- | --------- |
| propertyList | List<Dictionary<string, string>> | 对象及对象属性列表 |
| projectId    | string                           | 项目Id      |
| userInfo     | LoginUser                        | 用户信息      |

#### 返回说明

```
返回执行信息列表List<Dictionary<string, string>>，包含对象id及执行结果的值
```

#### 示例

    输入数据：
    propertyList:[{"_valm_Name":"Task1","_valm_NodeType":"Task"},
    {"_valm_Name":"Task2","_valm_NodeType":"Task"}]
    ----------------------------------------------------------------------
    输出结果：
    [{"_valm_Uid": "22123"},{"_valm_Uid": "22124"}]

## 批量标记删除【MultipleDelNode】

#### 参数说明

| 参数       | 类型        | 说明      |
| -------- | --------- | ------- |
| uids     | List<int> | 对象uid列表 |
| userInfo | LoginUser | 用户信息    |

#### 返回说明

```
返回执行结果
```

#### 示例

    输入数据：
    ["13321","11231","14431"]
    userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    -----------------------------------------------------------------------
    输出结果：
    [{"13321,"success","11231":"没有删除权限","14431":"success"}]

## 批量更新对象【MultipleModifyNode】

#### 参数说明

| 参数           | 类型                              | 说明     |
| ------------ | ------------------------------- | ------ |
| propertyList | List<Dictionary<string,string>> | 对象属性列表 |
| userInfo     | LoginUser                       | 用户信息   |

#### 返回说明

```
返回执行结果
```

#### 示例

    输入数据：
    propertyList：[{"_valm_Uid":"22123","_valm_NodeType":"Task"},
    {"_valm_Uid":"22124","_valm_NodeType":"Task"}]
    userInfo:用户信息LoginUser(UserName:user,UserPassword:123)
    -----------------------------------------------------------------------
    输出结果：
    [{"22123,"success",22124":"success"}]

## 批量增删改查【MultipleTransaction】

#### 参数说明

| 参数      | 类型                              | 说明     |
| ------- | ------------------------------- | ------ |
| proList | List<Dictionary<string,string>> | 对象属性列表 |
| proId   | int                             | 项目id   |
| user    | User                            | 用户信息   |

#### 返回说明

```
返回执行结果List<Dictionary<string,string>
```

#### 示例

    输入数据：
    proList:[{"[MultipleCRUD]":"Update","_valm_Uid":"22123","_valm_Name":
    "Task99","_valm_NodeType":"Task"},{"[MultipleCRUD]":"Add","_valm_Name":
    "Task33","_valm_NodeType":"Task"},{"[MultipleCRUD]":"Delete",
    "_valm_Uid":"22123","_valm_NodeType":"Task"}]
    
    proId:11306
    user:用户信息LoginUser(UserName:user,UserPassword:123)
    ----------------------------------------------------------------------
    输出结果：
    [{"_valm_Uid",22123,"ReturnMsg":"success"},{"_valm_Uid",22126,
    "ReturnMsg":"success",{"_valm_Uid",22123,"ReturnMsg":"success"}]

#### 备注

不可触发策略

## 分段获取文件【ResumeGetFileByCode】

#### 参数说明

| 参数       | 类型     | 说明        |
| -------- | ------ | --------- |
| FileCode | string | file Code |
| Position | long   | 位置        |
| ReadSize | int    | 读取的大小     |

#### 返回说明

```
返回文件信息ResumeGetFileByCode
```

#### 示例

    输入数据：
    FileCode："7aee461292c24ad0"
    Position：0
    ReadSize：0
    -------------------------------------------------------------------
    输出结果：
    {"fileData":null,"filename":"xssss","leftSize": 0}

## 查询产品库对应的配置项的版本数据【SearchCMStaticVersion】

#### 参数说明

| 参数                  | 类型                    | 说明   |
| ------------------- | --------------------- | ---- |
| searchConditionList | List<SearchCondition> | 查询条件 |
| returnProperties    | List<string>          | 返回属性 |
| user                | LoginUser             | 用户信息 |

#### 返回说明

```
返回查询到的配置项的版本数据
```

#### 示例

    输入数据：
    searchConditionList:[{"Property":"ProId","Condition":"=","PropertyVal":
    "11306","Conector":"and"},{"Property":"CMType","Condition":"=",
    "PropertyVal":"StaticRepo","Conector":"and"},{"Property":"UserVersion",
    "Condition":"=","PropertyVal":"V1","Conector":""}]
    
    returnProperties:["Uid","CMVersionUid","CMId","UserVersion","BranchName",
    "CreateBy"]
    user:用户信息LoginUser(UserName:user,UserPassword:123)
    ------------------------------------------------------------------------
    输出结果：
    [{"Uid":"12233","CMVersionUid":"221","CMId":"222",
    "UserVersion":"V1","BranchName":"test","CreateBy":"user"}]

## 查询开发库/受控库版本信息【SearchCM_Version】

#### 参数说明

| 参数                  | 类型                    | 说明  |
| ------------------- | --------------------- | --- |
| searchConditionList | List<SearchCondition> |     |
| returnProperties    | List<string>          |     |
| user                | 用户信息                  |     |

#### 返回说明

```
返回开发库/受控车的配置项版本信息
```

#### 示例

    输入数据：
    searchConditionList:[{"Property":"ProId","Condition":"=","PropertyVal":
    "11306","Conector":"and"},{"Property":"CMType","Condition":"=",
    "PropertyVal":"StaticRepo","Conector":""}]
    
    returnProperties:["Uid","CMId","AlmVersion","AdapterVersion",
    "UserVersion","BranchName"]
    user:用户信息LoginUser(UserName:user,UserPassword:123)
    
    ---------------------------------------------------------------------
    输出结果：
    [{"Uid":"12233","CMId":"222","AlmVersion":"V221","AdapterVersion":"V21",
    "UserVersion":"V1","BranchName":"test1"}]

## 获数据库唯一标识【SearchDBIdentity】

#### 参数说明

| 参数       | 类型     | 说明   |
| -------- | ------ | ---- |
| ip       | string | ip地址 |
| identity | string | 唯一标识 |

#### 返回说明

```
返回{"IPAddress":"","Identify":"","ReturnMsg":"success"}
```

#### 示例

    输入数据：
    ip:"172.1.1.133"
    identity:""
    ----------------------------------------------------------------------
    输出结果：
    {"IPAddress":"172.1.1.133","Identify":"","ReturnMsg":"success"}

## 查询文档管理的版本信息【SearchFileMVersion】

#### 参数说明

| 参数                  | 类型                    | 说明   |
| ------------------- | --------------------- | ---- |
| searchConditionList | List<SearchCondition> | 查询条件 |
| returnProperties    | List<string>          | 返回属性 |
| user                | LoginUser             | 用户信息 |

#### 返回说明

```
返回文档管理的版本信息IList<Dictionary<string,string>>
```

#### 示例

    输入数据：
    searchConditionList:[{"Property":"ProId","Condition":"=","PropertyVal":
    "11306","Conector":"and"},{"Property":"VersionType","Condition":"=",
    "PropertyVal":"FileCheckIn","Conector":""}]
    
    returnProperties：["Uid","VersionType","AdapterVersion","Status"]
    user：用户信息LoginUser(UserName:user,UserPassword:123)
    ----------------------------------------------------------------------
    输出结果：
    [{"Uid":"565421","VersionType":"FileCheckIn","AdapterVersion":"3",
    "Status":"0"},{"RowCount":"1","PageCount":"1"}]

## 获取类型【SearchNodeUserType】

#### 参数说明

| 参数           | 类型     | 说明     |
| ------------ | ------ | ------ |
| nodeTypeName | string | 数据类型名称 |

#### 返回说明

```
返回类型信息
```

## 查询单条数据，UID必传【SearchSingleNode】

#### 参数说明

| 参数                  | 类型                    | 说明   |
| ------------------- | --------------------- | ---- |
| searchConditionList | List<SearchCondition> | 查询条件 |
| returnProperties    | List<string>          | 返回属性 |
| userInfo            | LoginUser             | 用户信息 |

#### 返回说明

```
返回单条数据的信息Dictionary<string, string>
```

#### 示例

    输入数据：
    searchConditionList：[{"Property":"ProId","Condition":"=","PropertyVal":
    "11306","Conector":"and"},{"Property":"_valm_NodeType","Condition":"=",
    "PropertyVal":"Task","Conector":"and"},{"Property":"_valm_Uid",
    "Condition":"=","PropertyVal":"12221","Conector":""}]
    
    returnProperties：["_valm_Uid","_valm_Name","_valm_NodeType"]
    user：用户信息LoginUser(UserName:user,UserPassword:123)
    -----------------------------------------------------------------------
    输出结果：
    [{"_valm_Uid":"12221","_valm_Name":"Task2221","_valm_NodeType":"Task"}]

## 根据类型ID查询视图信息【SearchViewNodeTypeID】

#### 参数说明

| 参数         | 类型  | 说明     |
| ---------- | --- | ------ |
| nodeTypeID | int | 数据类型ID |

#### 返回说明

```
返回视图的列表信息
```

#### 示例

    输入数据：
    nodeTypeID：123
    -----------------------------------------------------------------------
    输出结果：
    [{"Uid":"123","Name":"VerisTest","Describe":"","NodeType":"Task",
    "ExTypeUid":"","ExType":"View","state":"Open","Check":"fasle"}]

## 根据枚举名称查询枚举项信息【SectionEnumDtlByEnumName】

#### 参数说明

| 参数       | 类型     | 说明   |
| -------- | ------ | ---- |
| enumName | string | 枚举名称 |

#### 返回说明

```
返回枚举项的具体信息
```

#### 示例

    输入数据：
    enumName："SofywareType"
    
    ------------------------------------------------------------------------
    输出结果：
    [{"Uid":"221","EnumValueKey":"kEY","EnumValueText":"KeySoft",
    "EnumManagerID":"2211","EnumFilterValue":"","EnumStyleValue":""}]

## 根据枚举ID和字段ID获取枚举【SectionEnumManagerUid】

#### 参数说明

| 参数         | 类型     | 说明   |
| ---------- | ------ | ---- |
| uid        | int    | 枚举Id |
| projectUid | int    | 项目id |
| memberUid  | string | 字段Id |

#### 返回说明

```
返回枚举信息
```

#### 示例

    输入数据：
    uid：2211
    projectUid：11306
    memberUid："221"
    -----------------------------------------------------------------------
    输出结果：
    {"Uid":"2211","Name":"aa","Description":"",
    "LastModifyDate":"2010-10-1 11:11:22","CreateBy":"user",
    "CreateData":"2010-10-1 11:11:22","MarkForDel":"false",
    "Sort":"1","EnumVale":"kEY21","EnumType":"","IsInner":""}

## 发送邮件【SendMail】

#### 参数说明

| 参数          | 类型                         | 说明     |
| ----------- | -------------------------- | ------ |
| entityId    | string                     | 对象Id   |
| emailTmplId | string                     | 邮件模板Id |
| userInfo    | LoginUser                  | 用户     |
| sendTo      | string                     | 收件人地址  |
| copyTo      | string                     | 抄送人地址  |
| mateData    | Dictionary<string, string> | 上下文数据  |

#### 返回说明

```
返回执行结果 true/false
```

#### 示例

    输入数据：
    entityId："12332121"
    emailTmplId："1231"
    userInfo：用户信息LoginUser(UserName:user,UserPassword:123)
    sendTo："user,user_k"
    copyTo："user1,user_k2"
    mateData：{}
    -----------------------------------------------------------------------
    输出结果：
    true

## 提交流程操作【SubmitFlow】

#### 参数说明

| 参数        | 类型        | 说明     |
| --------- | --------- | ------ |
| execValue | string    | 对象Id   |
| entityID  | string    | 邮件模板Id |
| userInfo  | LoginUser | 用户     |

#### 返回说明

```
返回执行结果 true/false
```

#### 示例

    输入数据：
    execValue："close_task"
    entityID:"22221"
    userInfo：用户信息LoginUser(UserName:user,UserPassword:123)
    -----------------------------------------------------------------------
    输出结果：
    true

## 上传文件【UplaodFile】

#### 参数说明

| 参数         | 类型                         | 说明                                        |
| ---------- | -------------------------- | ----------------------------------------- |
| buffers    | byte[]                     | 文件流                                       |
| properties | Dictionary<string, string> | 属性值列表{"FileName":"文件名","KeepTime":"保存时间"} |
| userInfo   | LoginUser                  | 用户                                        |

#### 返回说明

```
返回执行结果 {“ReturnMsg”:"success","ErrorMsg":"xxx",
"FileCode":“xxx”,"FilePath":"xxx"}
```

#### 示例

    输入数据：
    buffers：文件流byte[]
    properties：{"FileName":"","KeepTime":"2010-11-1 10:11:21"}
    userInfo：用户信息LoginUser(UserName:user,UserPassword:123)
    -----------------------------------------------------------------------
    输出结果：
    {"ReturnMsg":"failed","ErrorMsg":"数据流长度必须大于1，文件名不能为空",
    "FileCode":"","FilePath":""}

## 判断条件【ValidateCondition】

#### 参数说明

| 参数           | 类型        | 说明    |
| ------------ | --------- | ----- |
| conditionStr | string    | 条件字符串 |
| entityid     | string    | 对象ID  |
| userInfo     | LoginUser | 用户信息  |

#### 返回说明

```
返回执行结果 true/false
```

#### 示例

    输入数据：
    conditionStr：'{"ObjType":"DynRepoCheckIn","OrderBy":[],
    "Conditions":[{"Conector":"","Property":"OpResult",
    "DataType":"SINGLELINETEXT","EnumId":"","Condition":"14",
    "PropertyVal":"success","Groups":[]}]}'
    entityid："223554"
    userInfo：用户信息LoginUser(UserName:user,UserPassword:123)
    -----------------------------------------------------------------------
    输出结果：
    false
