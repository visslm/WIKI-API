# 文件管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 项目配置项文件资源信息

#### 接口地址：

    http://<server>/alm/rest/fm/project/{proId}/library/{type}/{fmId}/{branchName}/{*fileName}?UToken={utoken}

#### 请求方式：

    get

#### 参数输入：

| 参数         | 类型     | 是否必填 | 说明                  |
| ---------- | ------ | ---- | ------------------- |
| proId      | string | 是    | 项目Id                |
| type       | string | 是    | 版本类型(FileSign/Auto) |
| fmId       | string | 是    | File/Document的ID    |
| branchName | string | 是    | 文件名                 |
| fileName   | string | 是    | 用户的UToken           |
| UToken     | string | 是    | 用户令牌                |

#### 输出结果：

    返回JSON数据格式

## 项目配置项文件资源信息

#### 接口地址：

```
http://<server>/alm/rest/fm/project/{proId}/library/{type}/{fmId}/{branchName}?UToken={utoken}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数         | 类型     | 是否必填 | 说明                  |
| ---------- | ------ | ---- | ------------------- |
| proId      | string | 是    | 项目Id                |
| type       | string | 是    | 版本类型(FileSign/Auto) |
| fmId       | string | 是    | File/Document的ID    |
| branchName | string | 是    | 文件名                 |
| UToken     | string | 是    | 用户令牌                |

#### 输出结果：

```
返回JSON数据格式
```
