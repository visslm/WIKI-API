# 文档管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 查询任务管理数据

#### 接口地址：

    http://<server>/alm/rest/jm?UToken={utoken}&SearchCondition={searchCondition}&ReturnProp={returnProp}&PageSize={pageSize}&PageIndex={pageIndex}

#### 请求方式：

    post

#### 参数输入：

| 参数             | 类型     | 是否必填 | 说明   |
| -------------- | ------ | ---- | ---- |
| SearchCondtion | string | 是    | 查询条件 |
| ReturnProp     | string | 是    | 返回属性 |
| PageSize       | string | 是    | 页数   |
| pageIndex      | string | 是    | 页码   |
| UToken         | string | 是    | 用户令牌 |

#### 输出结果：

```
{"ErrorCode": 0, "ErrorMsg":"success",propList: []}
```

## 添加任务管理数据

#### 接口地址：

```
http://<server>/alm/rest/jm/Job/AddJM?UToken={utoken}
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数          | 类型                             | 是否必填 | 说明       |
| ----------- | ------------------------------ | ---- | -------- |
| UToken      | string                         | 是    | 用户令牌     |
| propertyDic | JSONDictionary<string, string> | 否    | 对象属性及属性值 |

#### 输出结果：

{"ErrorCode": 0, "ErrorMsg":"success",props: {}}

## 根据GUID更新任务管理数据

#### 接口地址：

```
http://<server>/alm/rest/jm/Job/{identifyGUid}/EditJM?UToken={utoken}
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数           | 类型                             | 是否必填 | 说明       |
| ------------ | ------------------------------ | ---- | -------- |
| identifyGUid | string                         | 是    | Guid     |
| UToken       | string                         | 是    | 用户令牌     |
| propertyDic  | JSONDictionary<string, string> | 否    | 对象属性及属性值 |

#### 输出结果：

true/false

## 根据GUID获取任务信息

#### 接口地址：

```
http://<server>/alm/rest/jm/Job/{identifyGUid}/Getjm?UToken={utoken}
```

#### 请求方式：

```
get/post/delete/put
```

#### 参数输入：

| 参数           | 类型     | 是否必填 | 说明   |
| ------------ | ------ | ---- | ---- |
| identifyGUid | string | 是    | GUID |
| UToken       | string | 是    | 用户令牌 |

#### 输出结果：

```
{"ErrorCode": 0, "ErrorMsg":"success",props: {}}
```

## 根据GUID获取任务状态信息

#### 接口地址：

```
http://<server>/alm/rest/jm/Job/{identifyGUid}/Status?UToken={utoken}
```

#### 请求方式：

```
get/post/delete/put
```

#### 参数输入：

| 参数           | 类型     | 是否必填 | 说明   |
| ------------ | ------ | ---- | ---- |
| identifyGUid | string | 是    | Guid |
| UToken       | string | 是    | 用户令牌 |

#### 输出结果：

{"ErrorCode": 0, "ErrorMsg":"success",props: {}}
