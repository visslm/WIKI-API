# 用户RestAPI接口说明

此接口为VISSLM提供, 接口实现形式为 HTTP Restful

\*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

    http://location:port/path?param=value#flag

用户登录
---------

    此为内部临时接口,随时取消

#### URI

     http://<server>/alm/rest/user/login/{user}/{password}

#### 方法

    GET

#### 参数

| 参数         | 说明    |
| ---------- |:----- |
| {user}     | 用户登录名 |
| {password} | 用户密码  |

#### 返回值

        {"ErrorCode":0,"ErrorMessage":null,"props":{"UToken":"UT4ce88ac904f04920b4746c9e786c54b7"}}

***

获取用户信息
---------

    根据令牌获取用户信息，此为内部临时接口,可能取消

#### URI

        http://<server>/alm/rest/user/info/{utoken}

#### 方法

    GET

#### 参数

| 参数       | 说明     |
| -------- |:------ |
| {utoken} | 用户身份令牌 |

#### 返回值

        {
         "AppName":"VISSLM_MAIN","GroupIds":"88,130,58,137,145","IP":"192.168.0.106","LicenseType":0,
         "NickName":"武则天","UToken":"UT4ce88ac904f04920b4746c9e786c54b7","Uid":170,"UserName":"Wu","UserType":0
        }

***

获取许可信息
---------

    获取当前许可服务信息

#### URI

        http://<server>/alm/rest/user/LicenseInfo

#### 方法

    GET

#### 返回值

    {
        "Float":{"ExpirationType":0,"MaxUserCount":2,"ExpiresTime":60,"TokenList":{}},
        "Static":{"ExpirationType":0,"ExpiresTime":60,"MaxUserCount":500,"TokenList"：{}}
    }

***

获取用户的API令牌
---------

获取指定用户的API令牌, 此为内部临时接口,可能取消

#### URI

        http://<server>/alm/rest/user/ApiToken/{utoken}

#### 方法

    GET

#### 返回值

    {"ErrorCode":0,"props":{"apiToken","Aadfasdfasgljksjdfsddasfasdffklss"}}

***

获取第三方登录会话句柄
---------

获取第三方登录会话句柄, 供后续接口使用

#### URI

        http://<server>/alm/rest/user/TPL/session/{appName}

#### 方法

    GET

#### 返回值

    {"ErrorCode":0,"props":{"TPLSession","Aadfasdfasgljksjdfsddasfasdffklss"}}

***

查询第三方登录授权状态
---------

使用之前获取的第三方登录会话句柄, 查询第三方登录授权状态

#### URI

        http://<server>/alm/rest/user/TPL/CheckAuth/{TPLSession}

#### 方法

    GET

#### 返回值

    {"ErrorCode":0,"props":{"UToken","Aadfasdfasgljksjdfsddasfasdffklss"}}

***

第三方验证用户登录
---------

使用之前获取的第三方登录会话句柄, 发送VISSLM用户登录名信息，进行第三方用户登录

#### URI

       http://<server>/alm/rest/user/TPL/login/{TPLSession}

#### 方法

    GET

#### 返回值

    {"ErrorCode":0}

***

# 用户管理RESTAPI接口说明 V2.0

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 用户登出

### 接口地址：

```
http://<server>/alm/rest/user/Logout/{utoken}
```

### 请求方式：

```
get
```

### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| utoken | string | 是    | 用户令牌 |

### 输出结果：

    无返回值

## 重设用户API UToken

### 接口地址：

```
http://<server>/alm/rest/user/ApiToken/{utoken}/Reset
```

### 请求方式：

```
get
```

### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| utoken | string | 是    | 用户令牌 |

### 输出结果：

    {"ErrorCode":"0","ErrorMessage":"","props": null}

## 获取长时间UToken

### 接口地址：

```
http://<server>/alm/rest/user/ApiXUToken
```

### 请求方式：

```
get
```

### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| utoken | string | 是    | 用户令牌 |

### 输出结果：

    {"ErrorCode":"0","ErrorMsg":"","Data": null}

## 获取已注册许可信息

### 接口地址：

```
http://<server>/alm/rest/user/ShowAllFeature
```

### 请求方式：

```
get
```

### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| utoken | string | 是    | 用户令牌 |

### 输出结果：

    "xxx"

## 获取服务器许可信息

### 接口地址：

```
http://<server>/alm/rest/user/ShowServerFeature
```

### 请求方式：

```
get
```

### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| utoken | string | 是    | 用户令牌 |

### 输出结果：

"    xxx"

## 新增用户

### 接口地址：

```
http://<server>/alm/rest/users/add
```

### 请求方式：

```
get
```

### 参数输入：

| 参数      | 类型               | 是否必填 | 说明                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ------- | ---------------- | ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| content | List<ClientUser> | 是    | ClientUser 用户信息，包括Uid，Name，Password，NickName，NickNamePinYin，Phone，Address，Mailbox，Sex，LastModifyDate，CreaeDate，MarkForDel，Enable 是否启用，Sort，IDType证件类型，IDNumber证件号，Autograph签名图片，AttributeExtension 扩展属性,AutographPassword 签名密码，Departent，LastModifyPwsTime，LoginFailCount，LoginFailTime，RoleJson 角色，ck 是否选择，CheckingLock 用户是否锁定，SecretType 密级管理类型，UserType 用户类型, KnowledgeProtectedLevel 知识保护等级，PermitType 许可类型，PermitExtension1 扩展属性1，PermitExtension2 扩展属性2，PermitExtension3 扩展属性3，DomainACCOUNT 域名,IPS IP集合，KnowUsers 知悉范围用户集合，ReviewUsers 评审范围用户,DigestPassword 摘要MD5密码UserFrozenReason 冻结原因,UserDepartment 用户所属部门,UserDepartmentIds List<string> 用户所属部门id,UserLanguageId 用户选择的语言,ArchivePassword 档案密码MD5,PublicUser 公共用户, License CilentUserLicense (SVNName, SVNPassword） |

### 输出结果：

    "xxx"

## 编辑用户

### 接口地址：

```
http://<server>/alm/rest/users/{username}
```

### 请求方式：

```
get
```

### 参数输入：

| 参数       | 类型     | 是否必填 | 说明   |
| -------- | ------ | ---- | ---- |
| username | string | 是    | 用户名  |
| utoken   | string | 是    | 用户令牌 |

### 输出结果：

    "xxx"