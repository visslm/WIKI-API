# 重用库RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 获取重用库内容(目前接口内容输出异常)

#### 接口地址：

    http://<server>/alm/rest/rp?UToken={utoken}

#### 请求方式：

    get

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    抛出未实例化异常，接口内容输出异常，目前无其他处理逻辑

## 获取项目重用库配置项列表(目前接口内容输出异常)

#### 接口地址：

```
http://<server>/alm/rest/rp/project/{proId}/library/{type}/
?UToken={utoken}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    抛出未实例化异常，接口内容输出异常，目前无其他处理逻辑

## 获取项目重用库分支列表(目前接口内容输出异常)

#### 接口地址：

```
http://<server>/alm/rest/rp/project/{proId}/library/{type}/
ci/{ciId}/?UToken={utoken}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    抛出未实例化异常，接口内容输出异常，目前无其他处理逻辑

## 获取重用件信息

#### 接口地址：

```
http://<server>/alm/rest/rp/project/{proId}/reusepartsId/
{reusepartsId}/reusepartsversion/{reusepartsversion}/ci/
{ciId}/civersion/{civersion}/{*path}?UToken={utoken}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数                | 类型     | 是否必填 | 说明    |
| ----------------- | ------ | ---- | ----- |
| proId             | string | 是    | 项目Id  |
| reusepartsId      | string | 是    | 重用件Id |
| reusepartsversion | string | 是    | 重用件版本 |
| ciId              | string | 是    | 配置项Id |
| civersion         | string | 是    | 配置项版本 |
| path              | string | 是    | 文件路径  |
| UToken            | string | 是    | 用户令牌  |

#### 输出结果：

    返回JSON格式{}

## 获取重用库资源(目前接口内容输出异常)

#### 接口地址：

```
http://<server>/alm/rest/rp/resource?target={target}&UToken={utoken}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| target | target | 是    |      |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    抛出未实例化异常，接口内容输出异常，目前无其他处理逻辑

## 获取重用库资源(目前接口内容输出异常)

#### 接口地址：

```
http://<server>/alm/rest/rp/resource/component/{compId}/library/
{type}/?UToken={utoken}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数     | 类型       | 是否必填 | 说明   |
| ------ | -------- | ---- | ---- |
| compId | string   | 是    | 组件Id |
| type   | string   | 是    | 类型   |
| UToken | `string` | 是    | 用户令牌 |

#### 输出结果：

    抛出未实例化异常，接口内容输出异常，目前无其他处理逻辑
