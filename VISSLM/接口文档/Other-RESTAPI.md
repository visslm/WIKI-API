# 缓冲等管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 清除缓冲

#### 接口地址：

    http://<server>/alm/rest/api/CacheClear

#### 请求方式：

    Get

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明    |
| ------ | ------ | ---- | ----- |
| keys   | string | 是    | 缓存KEY |
| UToken | string | 是    | 用户令牌  |

#### 输出结果：

    "success" / "exception message"

## 加载频繁的用户缓冲

#### 接口地址：

    http://<server>/alm/rest/api/LoadFrequentlyUsedCache

#### 请求方式：

    get

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    "ok"

## 重置扩展点缓存

#### 接口地址：

    http://<server>/alm/rest/api/ResetEPCache

#### 请求方式：

    get

#### 输入参数：

| 参数  | 类型  | 是否必填 | 说明  |
| --- | --- | ---- | --- |
|     |     |      |     |

#### 输出结果：

    "success"

## 拷贝条目化对象信息

#### 接口地址：

    http://<server>/alm/rest/api/CopyData

#### 请求方式：

    Post

#### 参数输入：

| 参数            | 类型     | 是否必填 | 说明                                                                                |
| ------------- | ------ | ---- | --------------------------------------------------------------------------------- |
| UToken        | string | 是    | 用户令牌                                                                              |
| paramCopyData | string | 是    | 参数Object，包含sourceList,  target, insertAfter, insertBefore, copyType, repeatImport |

#### 输出结果：

    class:ResultPropertyList   
    包含属性
    "ErrorCode":-1或0,
    "ErrorMessage":"xxx message"
    "propList"(List<JSONDictionary<string, string>>):""

## 刷新引用

#### 接口地址：

    http://<server>/alm/rest/api/RefreshReference

#### 请求方式：

    Get

#### 输入参数：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    {"ErrorCode": 0, "ErrorMsg":"",Data: true }

## 获取项目测试用例数据

#### 接口地址：

```
http://<server>/alm/rest/TestManager/project/{id}/TestCase?ReturnProperty={retProps}
```

#### 请求方式：

```
Get
```

#### 输入参数：

| 参数       | 类型     | 是否必填 | 说明   |
| -------- | ------ | ---- | ---- |
| id       | string | 是    | 项目id |
| retProps | string | 是    | 返回属性 |

#### 输出结果：

```
{"ErrorCode": 0, "ErrorMessage":"",Data: null}
```
