# 分析工具(适配器)RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 请求分析的回调接口

#### 接口地址：

    http://<server>/alm/rest/ToolAnalyzer/Analyse?UToken={token}

#### 请求方式：

    post

#### 参数输入：

| 参数     | 类型          | 是否必填 | 说明                                           |
| ------ | ----------- | ---- | -------------------------------------------- |
| UToken | string      | 是    | 用户令牌                                         |
| ad     | AnalyseData | 是    | 分析数据包括：toolid string ,id string ,data string |

#### 输出结果：

    {"result":"success","errorMessage":""}

## 回写状态

#### 接口地址：

```
http://<server>/alm/rest/ToolAnalyzer/CallbackStatus?UToken={token}
&toolId={toolId}&id={id}&statusCode={statuscode}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数         | 类型     | 是否必填 | 说明   |
| ---------- | ------ | ---- | ---- |
| toolId     | string | 是    | 工具ID |
| id         | string | 是    | key值 |
| statusCode | string | 是    | 状态码  |
| UToken     | string | 是    | 用户令牌 |

#### 输出结果：

    {"result":"success","errorMessage":""}

## 刷新UToken值

#### 接口地址：

```
http://<server>/alm/rest/ToolAnalyzer/RefreshToken?UToken={token}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明   |
| ------ | ------ | ---- | ---- |
| UToken | string | 是    | 用户令牌 |

#### 输出结果：

    {"result":"success","errorMessage":""}
