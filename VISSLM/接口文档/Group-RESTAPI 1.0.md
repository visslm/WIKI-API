# 用户组管理RestAPI接口说明

此接口为VISSLM提供, 接口实现形式为 HTTP Restful

\*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

    http://location:port/path?param=value#flag

根据用户组id获取用户组信息
---------

#### URI

        http://<server>/alm/rest/group/ID/{uid}

#### 方法

GET

#### 参数

| 参数       | 说明    |
| -------- |:----- |
| {uid}    | 用户组id |
| AppName  | 应用名称  |
| AppToken | 应用许可  |

#### 返回值

    {"ErrorCode":0,"ErrorMsg":"成功",
        "Data":{
            "Uid":34,
            "Name":"软件开发组",            
            "Description":"xxx",
            "LastModifyDate":"2022-04-02T14:40:21.923",
            "CreateBy":"user",
            ...
            }
    }

***

根据用户组名称获取用户组信息
---------

#### URI

        http://<server>/alm/rest/group/{groupName}

#### 方法

GET

#### 参数

| 参数          | 说明    |
| ----------- |:----- |
| {groupName} | 用户组名称 |
| AppName     | 应用名称  |
| AppToken    | 应用许可  |

#### 返回值

    {"ErrorCode":0,"ErrorMsg":"成功",
        "Data":{
            "Uid":34,
            "Name":"软件开发组",            
            "Description":"xxx",
            "LastModifyDate":"2022-04-02T14:40:21.923",
            "CreateBy":"user",
            ...
            }
    }

***

查询用户关联的用户组
---------

#### URI

        http://<server>/alm/rest/group/{userName}/Group

#### 方法

GET

#### 参数

| 参数         | 说明     |
| ---------- |:------ |
| {userName} | 用户登录名称 |
| AppName    | 应用名称   |
| AppToken   | 应用许可   |

#### 返回值

    {"ErrorCode":0,"ErrorMsg":"成功",
        "Data":[
            {
            "Uid":34,
            "Name":"软件开发组",            
            "Description":"xxx",
            "LastModifyDate":"2022-04-02T14:40:21.923",
            "CreateBy":"user",
            ...
            },
            {
            "Uid":35,
            "Name":"软件测试组",            
            "Description":"xxx",
            "LastModifyDate":"2022-04-02T14:40:21.923",
            "CreateBy":"user",
            ...
            }
        ]
    }

***

查询用户组成员
---------

#### URI

        http://<server>/alm/rest/group/{groupName}/Member

#### 方法

GET

#### 参数

| 参数          | 说明    |
| ----------- |:----- |
| {groupName} | 用户组名称 |
| AppName     | 应用名称  |
| AppToken    | 应用许可  |

#### 返回值

    {"ErrorCode":0,"ErrorMsg":"成功",
        "Data":[
            {
            "Uid":10305,"Name":"test1",
            ...
            },
            {
            "Uid":10306,"Name":"test2",
            ...
            }
        ]
    }

***

新增用户组
---------

可同时增加多个

#### URI

        http://<server>/alm/rest/group/Management/

#### 方法

POST

#### 参数

| 参数       | 说明   |
| -------- |:---- |
| AppName  | 应用名称 |
| AppToken | 应用许可 |

Content

    [
        {
            "Name" : "测试组1",
            "Description":"Desc"
        },
        {
            "Name" : "测试组2",
            "Description":"Desc"
        }
    ]

#### 返回值

    {
        "ErrorCode":0,"ErrorMsg":"成功"，
        "Data":{
            "SuccessAdd":[
                    "测试组1",
                    "测试组2"
                    ]，
            "FailAdd":[]
        }
    }

***

修改用户组
---------

修改部门信息

#### URI

        http://<server>/alm/rest/group/Management/{groupName}

#### 方法

PUT

#### 参数

| 参数          | 说明    |
| ----------- |:----- |
| {groupName} | 用户组名称 |
| AppName     | 应用名称  |
| AppToken    | 应用许可  |

Content

    {
        "Name":"测试组22",
        "Description":"Desc"
    }

#### 返回值

    {
        "ErrorCode":0,"ErrorMsg":"成功"
    }

***

删除用户组
---------

#### URI

        http://<server>/alm/rest/group/Management/{groupName}

#### 方法

DELETE

#### 参数

| 参数          | 说明    |
| ----------- |:----- |
| {groupName} | 用户组名称 |
| AppName     | 应用名称  |
| AppToken    | 应用许可  |

#### 返回值

    {
        "ErrorCode":0,"ErrorMsg":"成功"，
        "Data":null
    }

***

增加用户组成员
---------

可增加多个用户

#### URI

        http://<server>/alm/rest/group/{groupName}/Assignment/Append

#### 方法

POST

#### 参数

| 参数          | 说明    |
| ----------- |:----- |
| {groupName} | 用户组名称 |
| AppName     | 应用名称  |
| AppToken    | 应用许可  |

Content

    {
        "member":["test1","test2",...]
    }

#### 返回值

    {
        "ErrorCode":0,"ErrorMsg":"成功"
    }

***

删除用户组成员
---------

#### URI

        http://<server>/alm/rest/group/{groupName}/Assignment/Remove

#### 方法

POST

#### 参数

| 参数          | 说明    |
| ----------- |:----- |
| {groupName} | 用户组名称 |
| AppName     | 应用名称  |
| AppToken    | 应用许可  |

Content

    {
        "member":["test1","test2",...]
    }

#### 返回值

    {
        "ErrorCode":0,"ErrorMsg":"成功"，
    }

***

替换用户组成员
---------

此方法与增加用户组成员的区别是，会清空原有用户组成员

#### URI

        http://<server>/alm/rest/group/{groupName}/Assignment/Replace

#### 方法

POST

#### 参数

| 参数          | 说明    |
| ----------- |:----- |
| {groupName} | 用户组名称 |
| AppName     | 应用名称  |
| AppToken    | 应用许可  |

Content

    {
        "member":["test1","test2",...]
    }

#### 返回值

    {
        "ErrorCode":0,"ErrorMsg":"成功"，
    }

***