# 项目数据树形管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 获取节点信息

#### 接口地址：

    http://<server>/alm/rest/tree/{treeType}/id/{id}

#### 请求方式：

    get

#### 参数输入：

| 参数       | 类型     | 是否必填 | 说明    |
| -------- | ------ | ---- | ----- |
| treeType | string | 是    | pm/rm |
| id       | string | 是    | 对象id  |
| UToken   | string | 是    | 用户令牌  |

#### 输出结果：

    {"ErrorCode":"0","ErrorMessage":"操作成功","Data": null}

## 获取父节点信息

#### 接口地址：

```
http://<server>/alm/rest/tree/{treeType}/root
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数       | 类型     | 是否必填 | 说明    |
| -------- | ------ | ---- | ----- |
| treeType | string | 是    | pm/rm |
| UToken   | string | 是    | 用户令牌  |

#### 输出结果：

    {"ErrorCode":"0","ErrorMessage":"操作成功","Data": null}
