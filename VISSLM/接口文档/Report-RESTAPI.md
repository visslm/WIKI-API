# 报告管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 通用参数

所有接口的通用参数

LoginUser：用户信息

| 参数           | 说明        |
| ------------ | --------- |
| AppName      | 访问服务的应用名称 |
| UToken       | 用户登陆后令牌   |
| UserName     | 用户登录名     |
| UserPassword | 用户密码      |

> UToken与(UserName、UserPassword)二选一，同时存在时使用用户名与密码验证

## 获取报告信息

#### 接口地址：

    http://<server>/alm/rest/report/GetReport/{uid}

#### 请求方式：

    get

#### 参数输入：

| 参数  | 类型     | 是否必填 | 说明   |
| --- | ------ | ---- | ---- |
| uid | string | 是    | 报告id |

#### 输出结果：

    返回JSON格式，{"ErrorCode":0,"ErrorMsg":"", "Data": data }

## 获取报告额外信息

#### 接口地址：

```
http://<server>/alm/rest/report/GetReportOtherInfo/{uid}
```

#### 请求方式：

```
get
```

#### 参数输入：

| 参数  | 类型     | 是否必填 | 说明   |
| --- | ------ | ---- | ---- |
| uid | string | 是    | 报告id |

#### 输出结果：

    返回JSON格式，{"ErrorCode":0,"ErrorMsg":"", "Data": data }

## 生成报告(项目/组织/条目/文档容器/列表容器)

#### 接口地址：

```
http://<server>/alm/rest/report/project/{proId}/BuildReport?UToken={utoken}
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数          | 类型                 | 是否必填 | 说明     |
| ----------- | ------------------ | ---- | ------ |
| proId       | string             | 是    | 项目id   |
| UToken      | string             | 是    | 用户令牌   |
| propertyDic | Map<string,string> | 否    | 属性及属性值 |

#### 输出结果：

    返回JSON格式，{"ErrorCode":0,"ErrorMsg":"", "Data": {}}

## RelayMegre(VISSLM2.0)

#### 接口地址：

```
http://<server>/alm/rest/report/RelayMerge
```

#### 请求方式：

```
post
```

#### 参数输入：

| 参数    | 类型              | 是否必填 | 说明  |
| ----- | --------------- | ---- | --- |
| param | WordExportParam | 是    |     |

#### 输出结果：

    返回"XXX"
