# 组织资产管理RESTAPI接口说明

此接口为所有版本工具适配器公用, 包括VISSLM本身. 接口实现形式为 HTTP Restful

*返回值格式默认是"application/json", 本文中使用伪json示意。

URI格式说明

```
http://user:password@location:port/path?param=value#flag
```

## 资产入库申请

#### 接口地址：

    http://<server>/alm/rest/api/OrgAssetCommit?nodeId={uid}&UToken={utoken}

#### 请求方式：

    get

#### 参数输入：

| 参数     | 类型     | 是否必填 | 说明    |
| ------ | ------ | ---- | ----- |
| nodeId | string | 是    | 对象Uid |
| UToken | string | 是    | 用户令牌  |

#### 输出结果：

    class:ResultPropertyList   
    包含属性
    "ErrorCode":-1 或 0,
    "ErrorMessage":"xxx message"
    "propList"(List<JSONDictionary<string, string>>):null
